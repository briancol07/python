#!/bin/python3

#Given a two list of numbers create a new list such that new list should contain only odd numbers from the first list and even numbers from the second list

list1 = [10, 20, 23, 11, 17]
list2 = [13, 43, 24, 36, 12]
list3 = []
for elem in list1:
    if elem%2:
        list3.append(elem)

for elem in list2:
    if elem%2 ==0:
        list3.append(elem)

print(list3)
