#!/bin/python3

# Write a code to extract each digit from an integer, in the reverse order

def extractDigit(a):
    b = str(a)
    return b[::-1]


print(extractDigit(12))
print(extractDigit(1234565))
