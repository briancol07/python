#!/bin/python3 

# Print downward Half-Pyramid Pattern with Star (asterisk)

n = 5

for x in reversed(range(n+1)):
        print("*" * x)
