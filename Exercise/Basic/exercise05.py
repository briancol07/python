#!/bin/python3 

#Given a list of numbers, return True if first and last number of a list is same

lista = [1,2,3,4,5,6,7]
lista2 = [1,2,3,4,5,1]

print(lista[0] == lista[-1])
print(lista2[0] == lista2[-1])
