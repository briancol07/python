#!/bin/python3 

# Print multiplication table form 1 to 10


for x in range(11):
    for y in range(11):
        print("{} x {} = {}".format(x,y,x*y))
