#!/bin/python3

class Dog:
    def __init__(self, name, age):
        self.name = name 
        self.age = age 
    def description(self):
        return f"{self.name} is {self.age} years old"
    def speak(self, sound):
        return f"{self.name} says {sound}"

# This will create  an atribute call name and assigns the value 

pipo = Dog("Pipo",5)
pipa = Dog("Pipa",6)

print(pipo.description())
print(pipo.speak("miau miau"))
