#!/bin/python3

import json 
# json is javascript object 



json_string = '''

{
    "students":[
        {
            "id":1,
            "name":"Tim",
            "age":21
        },
        {
            "id":2,
            "name":"Joe",
            "age":22
        }
    ]

}
'''
# loads with s because string 
data = json.loads(json_string)
print(data['students'][0]['id'])

# dump data 
data['test'] = True 
new_json = json.dumps(data, indent = 4,sort_keys= True)
print(new_json)


