#!/bin/python3 

def Factorial(a):
   if a == 1:
    return a
   else:
    return a * Factorial(a-1)

def Estimacion():
  i = 1  
  for x in range(1,20):
    i = i + 1 / Factorial(x) 
  return i 


print(Estimacion())
