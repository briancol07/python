# Flujos de Seleccion 


18) 18)Escribir un programa que permita al usuario ingresar un número entero y luego muestre un mensaje indicando si el número es par o impar.

Pensando los pasos para resolver el problema:

    Pedir al usuario que ingrese un número entero.
    Almacenar ese número en una variable.
    Verificar si el número es par o impar.

    Si el número es par, mostrar un mensaje indicando que es par.

    Si el número es impar, mostrar un mensaje indicando que es impar.

(Para verificar si un número es par o impar, se puede utilizar el operador módulo (%). Si el resto de la división del número por 2 es 0, entonces el número es par. Si el resto de la división del número por 2 es 1, entonces el número es impar.)
19)Escribir un programa que permita ingresar dos números enteros e indicar si son iguales o distintos.
20)Escribir un programa que permita ingresar dos cadenas de caracteres e indicar si son iguales o distintas.
21)Escribir un programa que permita ingresar dos números enteros e indicar si el primero es mayor, menor o igual al segundo.
22)Escribir un programa que permita ingresar tres números enteros e indicar cual es el mayor.
23)Escribir un programa que permita ingresar tres números enteros y mostrar el mayor el menor y el valor que esta en medio.

Ejemplo: Si se ingresan los números 5, 3 y 7, el programa debe mostrar el número 5 como el menor, el número 7 como el mayor y el número 3 como el que esta en medio.
26)Escribir un programa que permita ingresar la cantidad de invitados a una fiesta y la cantidad de asientos disponibles en el salon. Debes indicar si alcanzan los asientos, Si los asientos no alcanzaran indicar cuántos faltan para que todos los invitados puedan sentarse.
27)Escribir un programa que permita ingresar una edad (entre 1 y 120 años), un género ('F'para mujeres, 'M' para hombres) y un nombre. En caso de haber ingresado valores erróneos (edad fuera de rango o género inválido), informar tal situación indicando el nombre de la persona. Si los datos están bien ingresados el programa debe indicar, sabiendo que las mujeres se jubilan con 60 años o más y los hombres con 65 años o más, si la persona está en edad de jubilarse.
28)Crear un programa que pida un número de mes (ejemplo 4) y escriba el nombre del mes en letras ("abril"). Verificar que el mes sea válido e informar en caso que no lo sea.
29)Escribir un programa que permita Ingresar las notas de los dos parciales de un alumno e indicar si promocionó, aprobó o debe recuperar. Si el valor de la nota no esta entre 0 y 10, debe informar un error.

    Se promociona cuando las notas de ambos parciales son mayores o iguales a 7.
    Se aprueba cuando las notas de ambos parciales son mayores o iguales a 4.
    Se debe recuperar cuando al menos una de las dos notas es menor a 4.

30)Escribir un programa que permita al usuario ingresar dos números enteros. La computadora debe indicar si el mayor es divisible por el menor.

(Un número entero a es divisible por un número entero b cuando el resto de la división entre a y b es 0)
31)
32)Una remisería requiere un sistema que calcule el precio de un viaje a partir de la cantidad de km que desea recorrer el usuario.

Tiene la siguiente tarifa:

    Viaje mínimo $50
    Si recorre entre 0 y 10km: $20/km
    Si recorre 10km o más: $15/km

Escribir un programa que permita ingresar la cantidad de km que desea recorrer el usuario y muestre el precio del viaje.

33)La farmacia Sindical efectúa descuentos a sus afiliados según el importe de la compra con la siguiente escala:

    Menor de $5500.0 el descuento es del 4.5%
    Entre $5500.0 y $10000.0 el descuento es del 8%
    Más de $10000.0 el descuento es del 10.5%

Escribir un programa que reciba un importe e informe: el descuento y el precio neto a cobrar, con mensajes aclaratorios.
34)
35)Existen dos reglas que identifican dos conjuntos de valores:

    a) El número es de un solo dígito.
    b) El número es impar.

A partir de estas reglas, escribir un programa que permita ingresar un número entero.

Debe asignar el valor que corresponda a las variables booleanas:

    esDeUnSoloDigito
    esImpar
    estaEnAmbos
    noEstaEnNinguno

el valor Verdadero o Falso, según corresponda, para indicar si el valor número ingresado pertenece o no a cada conjunto. Definir un lote de prueba de varios números y probr el algoritmo, escribiendo los resultados.
36)Escribir un programa que permita ingresar dos números enteros y la operación a realizar('+', '-', '*', '/'). Debe mostrarse el resultado para la operación ingresada. Considerar que no se puede dividir por cero (en ese caso mostrar el texto 'ERROR').
