# Guias de Entrada de datos 

## Ejercicio 1 
> Escribir un programa que permita que el usuario ingrese su nombre. El programa debe emitir una salida con un mensaje de bienvenida que incluya el nombre ingresado.

```
Ejemplo de ejecución:

Ingrese su nombre: Juan
Bienvenido Juan
```

## Ejercicio 2 

> Escribir un programa que solicite al usuario su nombre y su edad, y luego muestre por pantalla un mensaje que diga "Hola, [nombre]. Tu edad es [edad] años."
3. 

```
Ingrese su nombre: Juan
Ingrese su edad: 30
Hola, Juan. Tu edad es 30 años.
```

## Ejercicio 3 

> Escribir un programa que solicite al usuario su nombre y su edad, después pida una cantidad de años y muestre por pantalla un mensaje que indique cuántos años tendrá la persona después de sumarle a su edad la cantidad de años ingresada. El mensaje debe tener el siguiente formato: 'Hola, [nombre]. Dentro de [cantidad] años tendrás [edad + cantidad] años'".

```
Ejemplo: Si el usuario ingresa "Juan" y "20" y luego ingresa "5", el programa debe mostrar por pantalla "Hola, Juan. Dentro de 5 años tendrás 25 años".
```

## Ejercicio 4

> Escribir un programa que solicite al usuario ingresar tres numeros enteros.El programa debe mostrar por pantalla el resultado de sumar los tres numeros de la siguiente manera: "[numero1] + [numero2] + [numero3] = [suma]".

```
Si el usuario ingresa 1, 2 y 3, el programa debe mostrar por pantalla: "1 + 2 + 3 = 6".
```
## Ejercicio 5 

> Escribir un programa que solicite al usuario ingresar dos notas de un alumno. El programa debe mostrar por pantalla el promedio de las notas de la siguiente manera: "Notas: [nota1] , [nota2] ==> promedio: [(nota1+nota2)/2]".

```
Si el usuario ingresa 7 y 8, el programa debe mostrar por pantalla: "Notas: 7 , 8 ==> promedio: 7.5"
```
## Ejercicio 6 

> Escribir un programa que solicite al usuario ingresar tres notas de un alumno. El programa debe mostrar por pantalla las notas separadas por comas en un renglón y el promedio de las notas en el siguiente renglon.

```
Ingrese la nota 1: 7
Ingrese la nota 2: 8
Ingrese la nota 3: 9
Notas: 7, 8, 9
Promedio: 8.0
```

## Ejercicio 7 

> Escribir un programa que permita ingresar un número entero. Debe mostrarse el número ingresado:
> a. Multiplicado por 10. (utilizar el operador \*) a. Dividido por 10. (utilizar el operador /) a. Elevado al cuadrado. (utilizar el operador \*\*)
> Cada resultado debe mostrarse en una línea distinta.

```
Ingrese un número entero: 5
5 * 10 = 50
5 / 10 = 0.5
5 ** 2 = 25
```

## Ejercicio 8

> Escribir un programa que permita ingresar el valor monetario de una hora de trabajo y la cantidad de horas trabajadas por día, para calcular el salario semanal de un trabajador que trabaja todos los días hábiles y la mitad de las horas del día hábil los sábados, suponiendo que todas las horas tienen el mismo valor."


* Como pensarlo:
  1. Pedir al usuario que ingrese el valor monetario de una hora de trabajo y almacenarlo en una variable valor_hora.
  2. Pedir al usuario que ingrese la cantidad de horas trabajadas por día por el trabajador y almacenarla en una variable horas_trabajadas_por_dia.
  3. Calcular el salario diario del trabajador multiplicando valor_hora por horas_trabajadas_por_dia.
  4. Calcular el salario semanal del trabajador multiplicando el salario diario por la cantidad de días hábiles de la semana. Para esto, puedes utilizar la constante dias_habiles definida como 5.
  5. Calcular la cantidad de horas trabajadas por el trabajador el sábado, que es la mitad de la cantidad de horas trabajadas por día hábil. Para esto, se puede utilizar la vaiable horas_sabado definida como horas_trabajadas_por_dia / 2.
  6. Calcular el salario del trabajador por las horas trabajadas el sábado multiplicando valor_hora por horas_sabad
  7. Sumar el salario semanal con el salario del sábado para obtener el salario total semanal del trabajador.
  8. Mostrar el resultado del salario semanal en la pantalla.

## Ejercicio 9

> Escribir un programa que permita ingresar valores del mismo tipo para las variables num1 y num2. Una vez cargadas, mostrar ambas variables por pantalla, intercambiá sus valores (que lo cargado en num1 quede en num2, y viceversa) y volvé a mostrarlas actualizadas.

```
a=10
b=20
print(a,b)
a = a + b;
b = a - b;
a = a - b;
print(a,b)
``` 
## Ejercicio 10 

* Escribir un programa para resolver el siguiente problema:
Tres personas invierten dinero para fundar una empresa (no necesariamente en partes iguales). Calcular qué porcentaje invirtió cada una.
Como pensarlo:

* Solicitar al usuario que ingrese las cantidades invertidas por cada persona en tres variables numéricas.

```
inversion_persona1 = float(input("Ingrese la cantidad invertida por la persona 1: "))
inversion_persona2 = float(input("Ingrese la cantidad invertida por la persona 2: "))
inversion_persona3 = float(input("Ingrese la cantidad invertida por la persona 3: "))
```

* Calcular el total de la inversión sumando las cantidades de las tres personas.
  * total = inversion_persona1 + inversion_persona2 + inversion_persona3
* Calcular el porcentaje que representa la inversión de cada persona en relación al total de la inversión.
* Dividir la cantidad invertida por cada persona entre el total de la inversión y multiplicar por 100 para obtener el porcentaje. Almacenar el resultado en una variable correspondiente a cada persona. Opcionalmente, se puede redondear el resultado a un número determinado de decimales utilizando la función round(). 

```
porcentaje_inversion_persona1 = round((inversion_persona1 / total) * 100, 2) porcentaje_inversion_persona2 = round((inversion_persona2 / total) * 100, 2) porcentaje_inversion_persona3 = round((inversion_persona3 / total) * 100, 2)
```

## Ejercicio 11 

Escribir un programa que permita resolver el siguiente problema:

Tres personas aportan diferente capital a una sociedad y desean saber el valor total aportado y qué porcentaje aportó cada una (indicando nombre y porcentaje).

Solicitar la carga por teclado del nombre de cada socio y su capital aportado, a partir de esto calcular e informar lo requerido previamente.

## Ejercicio 12 

Escribir un programa en Python que solicite al usuario ingresar dos valores que representen las medidas en grados de dos ángulos interiores de un triángulo. Luego, calcular y mostrar por pantalla el valor en grados del ángulo restante.

Es importante recordar que la suma de los ángulos interiores de todo triángulo es de 180 grados. Es decir, la suma de los ángulos internos de un triángulo siempre es igual a 180 grados. Por lo tanto, para calcular el ángulo restante es necesario restar la suma de los dos ángulos interiores ingresados al valor 180."

* Para pensar:
  * ¿Qué pasaría si se ingresan valores negativos como medidas de ángulos?
  * ¿Qué sucedería si la suma de los dos ángulos ingresados es mayor o igual a 180 grados?


## Ejercicio 13 

Escribir un programa para calcular el importe a cobrar por un vendedor, considerando su sueldo fijo de $200000 pesos más el 16% del monto total de ventas realizadas durante un mes.
Pensando los pasos para resolver el problema:

    Solicitar al usuario que ingrese el monto total de ventas realizadas por el vendedor durante el mes en una variable correspondiente.

    Calcular el 16% del monto total de ventas realizadas y almacenar el resultado en una variable.

    Sumar el resultado del cálculo anterior al sueldo fijo del vendedor.

    Mostrar el importe a cobrar por el vendedor.

* Para pensar:
  * ¿Qué pasaría si se modificara el sueldo fijo del vendedor?
    * Si se modifica el sueldo fijo del vendedor, entonces la fórmula utilizada para calcular el salario total debería ser actualizada para reflejar el nuevo sueldo fijo. En este caso, si el sueldo fijo aumenta, entonces el salario total también aumentaría. De igual manera, si el sueldo fijo disminuye, entonces el salario total también disminuiría. Es importante actualizar la fórmula en el programa para asegurarse de que el cálculo del salario total sea preciso y refleje el cambio en el importe a cobrar por del vendedor.
  * ¿Hay que modificar el programa cada vez? ¿Cómo lo soluciono?  

## Ejercicio 14

Escribir un programa que permita al usuario ingresar el ancho y largo de un terreno en metros, junto con el valor del metro cuadrado de tierra. El programa debe calcular y mostrar el valor total del terreno. Además, debe calcular la cantidad de metros de alambre necesarios para cercar completamente el terreno a tres alturas distintas.
Pensando los pasos para resolver el problema:

    Solicitar al usuario que ingrese el ancho del terreno en metros y almacenarlo en una variable. Solicitar al usuario que ingrese el largo del terreno en metros y almacenarlo en otra variable.
    Solicitar al usuario que ingrese el valor del metro cuadrado de tierra y almacenarlo en otra variable. Calcular el valor total del terreno multiplicando el ancho por el largo y luego multiplicando el resultado por el valor del metro cuadrado de tierra.
    Mostrar el valor total del terreno al usuario.
    Calcular la cantidad de metros de alambre necesarios para cercar el terreno a tres alturas distintas. Por ejemplo, se puede calcular la cantidad de alambre necesaria para cercar a 1 metro de altura, a 2 metros de altura y a 3 metros de altura. Para hacerlo, se debe sumar el perímetro del terreno (2 veces el ancho más 2 veces el largo) y luego multiplicarlo por la cantidad de alturas. Mostrar la cantidad de metros de alambre necesarios para cercar el terreno a las tres alturas distintas al usuario.


## Ejercicio 15

Definición del problema: Una inmobiliaria paga a sus vendedores un salario base, más una comisión fija por cada venta realizada, más el 5% del valor de esas ventas. Realizar un programa que imprima el nombre del vendedor y el salario que le corresponde en un determinado mes.

Se leen por teclado el nombre del vendedor, la cantidad de ventas que realizó y el valor total de las mismas.

¿Sobran datos? ¿Qué datos sobran?

## Ejercicio 16

Escribir un programa que permita al usuario ingresar un período de tiempo en segundos. Luego, el programa debe convertir ese período de tiempo a una forma más legible y comprensible para el usuario, expresando el resultado en días, horas, minutos y segundos. El resultado se mostrará en pantalla en un mensaje que indique la cantidad de segundos ingresados y su equivalente en días, horas, minutos y segundos.

```
Ejemplo: 200000 segundos equivalen a 2 días, 7 horas, 33 minutos y 20 segundos. Usar en el programa las siguientes instrucciones:

dias = segundos // 86400 # 86400 segundos = 1 día

horas = (segundos % 86400) // 3600 # 3600 segundos = 1 hora

minutos = (segundos % 3600) // 60 # 60 segundos = 1 minuto

segundos_restantes = segundos % 60 # segundos restantes
```

## Ejercicio 17

¡Ayuda! Se me rompió el programa que convierte una cantidad de dinero en la cantidad mínima de billetes y monedas necesarios. Tengo todas las instrucciones necesarias, pero están todas mezcladas. ¿Podrías ayudarme a ordenarlas de manera correcta para que funcione el programa como debería? A lo mejor se me perdieron algunas instrucciones, ¿podrías agregarlas?

```
resto = cantidad_total
billetes_cien = resto // 100
resto = resto % 100
billetes_cinco = resto // 5
billetes_mil = resto // 1000
billetes_cincuenta = resto // 50
billetes_doscientos = resto // 200
billetes_diez = resto // 10
billetes_docientos = resto // 200
resto = resto % 10
cantidad_total = int(input("Ingrese la cantidad de dinero a convertir: "))
billetes_uno = resto // 1
print("Para la cantidad de  ",𝑐𝑎𝑛𝑡𝑖𝑑𝑎𝑑𝑡𝑜𝑡𝑎𝑙,"𝑠𝑒𝑛𝑒𝑐𝑒𝑠𝑖𝑡𝑎𝑛:")𝑝𝑟𝑖𝑛𝑡(𝑏𝑖𝑙𝑙𝑒𝑡𝑒𝑠𝑚𝑖𝑙,"𝑏𝑖𝑙𝑙𝑒𝑡𝑒𝑠𝑑𝑒 1000")
print(billetes_doscientos, "billetes de  200")𝑝𝑟𝑖𝑛𝑡(𝑏𝑖𝑙𝑙𝑒𝑡𝑒𝑠𝑐𝑖𝑒𝑛,"𝑏𝑖𝑙𝑙𝑒𝑡𝑒𝑠𝑑𝑒 100")
print(billetes_cincuenta, "billetes de  50")𝑝𝑟𝑖𝑛𝑡(𝑏𝑖𝑙𝑙𝑒𝑡𝑒𝑠𝑑𝑖𝑒𝑧,"𝑏𝑖𝑙𝑙𝑒𝑡𝑒𝑠𝑑𝑒 10")
print(billetes_cinco, "billetes de  5")𝑝𝑟𝑖𝑛𝑡(𝑏𝑖𝑙𝑙𝑒𝑡𝑒𝑠𝑢𝑛𝑜,"𝑏𝑖𝑙𝑙𝑒𝑡𝑒𝑠𝑑𝑒 1")
```
