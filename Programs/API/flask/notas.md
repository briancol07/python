# Notas de api 

* Posibles API
  * [Retrofit](https://square.github.io/retrofit/)
  * Python 
    * [FastAPI](https://fastapi.tiangolo.com/)
    * [Flask](https://fastapi.tiangolo.com/)

## The Choosen one   

Por ahora la que elegimos seria flask, tnego que probar las otras y ver cual es la que convine mas o es mas facil

* Instalacion
  * windows 
    * pip install flask
  * Linux 
    * pip3 install flask


## Iniciar la api

```python
app.run(debug=True)
```

Agregar decorator
el @app es para decir que nuestra api que creamos antes 

```python
# / es la ruta default
@app.route("/")
```

## Metodos 

* GET 
  * Request data from a specified resource 
* POST
  * Create a resource 
* PUT
  * Update a resource 
* DELETE 
  * Delete a resource 

## API basica 

```python

#!/bin/python3

from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route("/")

def home():
  return "HOME"

if __name__ == "__main__":
  app.run(debug=True)
```

## GET

"/get-user/" 
to get the value, path parameters

query parameter 

"/get-user/123?extra=hhello world" 

