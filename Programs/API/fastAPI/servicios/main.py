from typing import Union
from fastapi import FastAPI
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from typing import List, Optional
from datetime import datetime
from fastapi import FastAPI, HTTPException
import databases
import sqlalchemy
from sqlalchemy import create_engine, Column, Integer, String, DateTime
from sqlalchemy.sql import func
from databases import Database


DATABASE_URL = "sqlite:///./servicio.db"
database = Database(DATABASE_URL)
metadata = sqlalchemy.MetaData()


app = FastAPI()

class User(BaseModel):
  id:int
  name:str 
  points:int = 5 # This make the defualt value to 5
  trustGrade:str 
  # No confiable, Con reservas, Confiable Nivel 1, Confiable Nivel 2 


class Incident(BaseModel):
  title:str
  description:str 
  openBy:str
  closeBY:str 
  timeStampt:datetime 

class OnlytrustGrade(BaseModel):
  trustGrade : str 
#------------------------------------------ DB -------------------------------------------------------
users = sqlalchemy.Table(
    "users",
    metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column("name", String, unique=True, index=True),
    Column("poihts", Integer),
    Column("trustGrade", String),
)

@app.on_event("startup")
async def startup():
    await database.connect()

@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()

# @app.post("/users/", response_model=UserInDB)
# async def create_user(user: UserCreate):
#     query = users.insert().values(username=user.username, email=user.email)
#     user_id = await database.execute(query)
#     return {**user.dict(), "id": user_id}


#------------------------------------------ Sample data ----------------------------------------------

users_db = [] 
incident_db = [] 

# ------------------------------------------ USER -------------------------------------------------------

@app.post("/users/", response_model = User)
def create_user(user: User):
  users_db.append(user)
  return user

@app.get("/users/{user_id}", response_model=User)
def read_user(user_id: int):
  if user_id < 0 or user_id >= len(users_db):
    raise HTTPException(status_code=404, detail="User not found")
  return users_db[user_id]

@app.delete("/users/{user_id}", response_model=User)
def delete_user(user_id: int):
  if user_id < 0 or user_id >= len(users_db):
    raise HTTPException(status_code=404, detail="User not found")
  deleted_user = users_db.pop(user_id)
  return deleted_user

# ------------------------------------------- Incident ---------------------------------------------------

@app.post("/incident/", response_model = Incident)
def create_incident(incident: Incident):
  incident_db.append(incident)
  return incident

@app.get("/incident/{incident_id}", response_model = Incident)
def get_incident(incident_id: int):
  if incident_id < 0 or user_id >= len(incident_db):
    raise HTTPException(status_code=404, detail="Incident not found")
  return incident_db[incident_id]


# ----------------------------------------------------------------------------------------------------------
@app.get("/user/trustGrade")
def get_trust_Grade(user_index:int):
  if user_index < 0 or user_index >= len(user_db):
    return "user not found"
  trustGrade = user_db[user_index].trustGrade 
  return trustGrade
    
    
def calculate_trust_grade():
  pass 

@app.get("/")
def read_root():
    return {"Hello": "World"}


