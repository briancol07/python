from django.contrib import admin
from .models import Drink

# To register the models
admin.site.register(Drink)
