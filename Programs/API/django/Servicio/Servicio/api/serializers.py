from rest_framework import serializers
from .models import * 

## Serializer for User

class UserSerializer(serializers.ModelSerializer):
  class Meta: 
    model = User 
    fields = '__all__'

## Serializer for Incidents 

class IncidentsSerializer(serializers.ModelSerializer):
  class Meta: 
    model = User 
    fields = '__all__'

# Serializer for TrustGrade 

class TrustGradeSerializer(serializers.ModelSerializer):
  class Meta: 
    model = User 
    fields = '__all__'
