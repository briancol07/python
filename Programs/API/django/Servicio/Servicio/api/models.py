from django.db import models 

## Here write models 

class User(models.Model):
  name = models.CharField(max_length = 100)
  points = models.DecimalField(max_digits = 5, decimal_places = 2)
  trustGrade =  models.ForeignKey(TrustGrade,on_delete = models.CASCADE)

  def __str__(self):
    return self.name 

  def __init__(self):
    self.points = 5 
  
  def points(self,value):
    self.points = value 

class Incidents(models.Model):
  title = models.CharField(max_length = 200)
  createdBy = models.ForeignKey(User, on_delete = models.CASCADE)
  date = models.DateField()
  description = models.CharField(max_length = 200)

  def __str__(self):
    return self.title

class TrustGrade(models.Model):
  
  grade = models.CharField(max_length = 20);
  user = models.ForeignKey(User, on_delete = models.CASCADE)
  
  def calculateGrade(self):
    return grade
    


