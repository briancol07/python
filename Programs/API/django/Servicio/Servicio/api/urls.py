from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import *

from rest_framework.urlpatterns import format_suffix_patterns

router = DefaultRouter()
router.register(r'user',UserViewSet)
router.register(r'incidents',IncidentViewSet)
router.register(r'trustGrade',TrustGradeViewSet)


urlpatterns = [ 
  path('', include(router.urls)),
  path('user/',views.UserViewSet)
]

urlpatterns = format_suffix_patterns(urlpatterns)
