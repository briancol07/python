# API with django 

First we create an virtual environment to don't have crush dependecies

```python
# venv = virtual environment 
# .venv is the name that we gave 
python3 -m venv .venv 
```

```python
pip install django
pip installdjangorestframework
```

## django

Usefull stuff for your application


```python
django-admin
```

## Start server 

This will create a folder with the name of it in the current directory 
```python
django-admin startproject name .
```

This command will start the proyect (server)
```python
python manage.py runserver 
```
Apply all the migration

```python
python manage.py migrate
# to make the models in database 
python manage.py makemigrations name
```

```python
python manage.py createsuperuser
```

## Create a model 

We have to inherit from model class, create model.py with this inside 


```python

from django.db import models 

class Drink(models.Model):
  name = models.CharField(max_length=200)
  description = models.CharField(max_length=500)
```

now we have to migrate but first we need to add to settings.py, in the part of installed_apps before we migrate 

This will create a database.

To finaly show up the tables in the page we will have to create  file called admin.py
This is help full but no necesary 


```python

from django.contrib import admin
from .models import Drink

# To register the models
admin.site.register(Drink)
```

## Add rest framework 

in settings.py we add rest_framework

## Serializer 

To get json from python 

```python

from rest_framework import serializer 

class DrinkSerializer(serializer.ModelSerializer):
  # metadata 
  class Meta:
    model = Drink 
    fields = ['id','name','description']
```


## view

This is were all endpoint will be 

* get all drinks 
* serialize them
* return json 

```python

from django.http import JsonResponse
from .models import Drink
from .serializers import DrinkSerializer

def drink_list(request):
  # get all drinks
  drinks = Drink.objects.all()
  # seriale them
  serializer = DrinkSerializer(drinks,many=True) 
  # return json 
    
  return JsonResponse(serializer.data,safe=False)
```

Now we have to say what url we are gonna use 
