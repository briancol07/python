# API

Is a connector between two applications

## Folders 

```
|- Folder  
  |- init.py
  |- asgi.py
  |- settings.py
  |- urls.py
  |- wsgi.py
|- manage.py 
|- Migrations 
  |- admin.py
  |- apps.py
  |- models.py
  |- test.py
  |- views.py
  
```

* Settings.py
  * If you add a new app you have to add it here 
* Url.py 
  * Here are all the links of pages and admin
  * Admin is default 
* wsgi.py
  * Is the server running the application 
* models.py
* manage.py 
  * All the commands to run our app 

## Note

if your app have alot of queries you should change sqlite 3 to another one 


