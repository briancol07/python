#!/bin/python3 

abc = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' ]

def is_in_alphabet(ch):
  return (ch > 'a' and ch < 'z') or  (ch > 'A' and ch < 'z')

def add_variables():
  var = input("ingrese la key")
  result = list(filter(is_in_alphabet,var))
  while len(result) != len(var):
    var = input("ingrese la key")
    result = list(filter(is_in_alphabet,var))
  return result 

def position(a):
  return abc.index(a)+1

def check_numbers(lst):
  return list(map(position,lst))

def split_text(n,line):
  return [line[i:i+n] for i in range(0, len(line), n)]

def text_to_list(lst):
  text = "".join(input("ingrese la frase a cifrar").split(" "))
  print(text)
  aux_list = split_text(len(lst),text)
  return aux_list

def cipher(text,key = 3):
  aux_list = "" 
  for x in text:
    for j in x:
     aux_list+=(chr(ord(j)+key))
  return aux_list
      
  
def main():
  nums = check_numbers(add_variables())
  aux = text_to_list(nums)
  auxlist = cipher(aux)
  print(nums)
  print(aux)
  print(auxlist)

main()
