#!/bin/python3 

def cantVecesLista(lista,item):
  cont = 0 
  # Recorro los elementos de la lista y veo cuales son iguales a item 
  for elem in lista:
    if elem == item:
      cont +=1 
  return cont  

def posicionesQueEsta(lista,item):
  posiciones = []
  # Recorro cada posicion y veo si el elemento de la lista en esa posicion es igual a item entonces guardo esa posicion
  for x in range(len(lista)):
    if lista[x] == item:
      posiciones.append(x)
  return posiciones 


def main():
  lista = ['a','x','a','a','c','b']
  print("La Cantidad de veces que se repite a en la lista son : " ,cantVecesLista(lista,'a'))
  print("Las posiciones que a esta en la lista son : " , posicionesQueEsta(lista,'a'))


main()


      




