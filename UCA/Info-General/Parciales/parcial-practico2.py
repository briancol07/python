#!/bin/python3 


def esLetra(char):
  res = False 
  if char >= 'a' and char <= 'z' or char >= 'A' and char <= 'Z':
    res = True 
  return res
   
  
def texto_a_Lista(texto):
#  texto = "la papa es plantada en la tierra."
  lista_palabras = [] 
  palabra_aux = ""
  for elem in texto: 
   if esLetra(elem): 
     palabra_aux += elem
   else:
     lista_palabras.append(palabra_aux)
     palabra_aux = ""
  return lista_palabras
  

def contarVocales(palabra):
  cont_Abiertas = 0
  cont_Cerradas = 0 
  res = False
  for elem in palabra: 
    if elem in "aeoAEO": 
      cont_Abiertas +=1 
    if elem in "iuIU": 
      cont_Cerradas +=1 
  if cont_Abiertas == cont_Cerradas :
    res = True
  return res

def maximo(a,b):
  res = b
  if len(a) > len(b):
    res = a 
  return res 

def lista_iguales_vocales(lista):
   # [alpha, beta] --> 0 - alpha y 1 - beta 
  lista_aux = []
  for elem in lista: 
    if contarVocales(elem):
      lista_aux.append(elem)
  return lista_aux

def maxima(texto):
#   "puchito,papa,pipo" 
  lista_aux = []
  lista_aux = texto_a_Lista(texto)
  print(lista_aux)
  lista_aux = lista_iguales_vocales(lista_aux)
  print(lista_aux)
  maxim = "" 
  for elem in lista_aux:
    maxim = maximo(maxim,elem)
  print(maxim)
  return maxim
    

def main():
  print(maxima("pucho,-,pucherito-nito^carlin"))

main()
