#!/bin/bash 

'a' 
"a"
       # 01234567891234567891234
texto = "el sapo pepe salta alto"
lista = ['e','l']
textoPor4 = (texto+" ") * 4

#print(textoPor4)

# recorrerlas 

for elem in texto:
  print("elem = ",elem)
        #      (0,20)

for x in range(len(texto)):
  print("x = ",x,f"el valor texto[{x}] = ",texto[x])

#len(texto)

        # 01234567891234567891234
#texto = "el sapo pepe salta alto"

# =================================================================

# arrays 
array1 = [ 4,True,'a']
print(array1)
print("Primer elemento",array1[0])
print("segundo elemento",array1[1])
print("tercer elemento",array1[2])

# slices 
#texto[3:6] --> sapo 
#texto[::-1]
#texto[inicio:fin:comoRecorro]
# fin es opcional si voy a recorrerlo hasta el final
# comoRecorro es opcional si voy de 1 en 1 
# inicio es opcional si queremos desde inicio hasta un valor
print(texto[19:])
print(texto[24:18:-1])
print(texto[:18:-1])
# desde el inicio hasta la letra 11 y que vaya de dos en dos 
#texto[inicio:fin:comoRecorro]
print(texto[ :11:2])
