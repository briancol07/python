# Informatica General 

## Overview 

> Guias 

* [Guia-01-In-Out](./Guia-01-In-Out)
* [Guia-02-Funciones](./Guia-02-Funciones)
* [Guia-03-Condicionales](./Guia-03-Condicionales)
* [Guia-04-Ciclos](./Guia-04-Ciclos)
* [Guia-05-Cadenas-Char](./Guia-05-Cadenas-Char)
* [Guia-06-Conjuntos](./Guia-06-Conjuntos)
* [Ejercicios](./Ejercicios)
* [Parciales](./Parciales)


