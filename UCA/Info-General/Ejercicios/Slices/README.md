# Slices 

## Ejercicios 

* Cargar una lista de 10 numeros randoms 
* Comparar dos listas y decir cual es la mayor 
* Invertir una lista 
* De una lista determinar cual es el mayor numero 
* De otra lista determinar cual es el menor 
* Determinar si un numero esta en la lista 
  * Utilizando IN
  * Utilizando while 
* De un lista eliminar los duplicados 
* De otra lista contar los duplicados y cuales son 
* Contar la cantidad de letras en un textos 
* Obtener una porcion de dos numeros ingresados por el usuario 
* De un texto obtener las palabras 
* Determinar si una lista es capicua 
* Recortar una lista desde el inicio al numero ingresado por el usuario 
