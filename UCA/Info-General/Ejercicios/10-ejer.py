#!bin/python 

def esPar(n): # si el numero es par (devuelve true o false)
  res = False 
  if n%2==0:
    res = True
  return res 

def multiplicaPares():
  a = 3 
  b = 7 
  # que vaya de a a b --> (3,4,5,6,7)
  # que vaya de b a a --> (7,6,5,4,3) 

  multi = 1    # Variable para multiplicar --> Inicializada en 1 por que es el neutro de multiplicacion  

  while a <= b: # 3 ,4 ,5, 6 ,7 
  # a es el contador 
  # print(a)
    if esPar(a) : # puede ir esPar(a) == True 
      multi *= a  # 1 * 4 , 4 * 6 
    a +=1 
  return multi 

def main():
  print(multiplicaPares())

main()
  

