#/bin/python3 

def maxi(a,b):
  return a if a > b else b 

def mini(a,b):
  return a if a < b else b 

def funcion1():
  maximo = 0 
  minimo = 1 
  num = int(input("Ingrese un numero \n"))
  while(num>0):

    maximo = maxi(num,maximo)
    minimo = mini(num,minimo)

    num = int(input("Ingrese un numero \n"))

  print("El maximo es {}".format(maximo))

  
  print("El minimo es {}".format(minimo))
  
  return maximo , minimo 

def main():
  funcion1()

main()
  
