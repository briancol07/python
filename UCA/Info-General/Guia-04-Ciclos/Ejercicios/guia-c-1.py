#/bin/python3 


# Ejercicio 1 
def mostrar1a100():
  for i in range(1,100):
    print("el numero es {}".format(i))

## Ejercicio 2 

def tablaDel6():
  for i in range(1,10):
    print("6 x {} = {}".format(i,6*i))

def mostrarLetras():
  letra = 'a'
  
  for i in range(1,27):
    print("la letra es {}".format(chr(ord(letra)+i)))
   

# ord(caracter) --> codigo decima ascii 
# chr(intero) ---> simbolo 

def main():
# Ejercicio 1 
  mostrar1a100()
# Ejercicio 2 
  tablaDel6()
# Ejercicio 3
  mostrarLetras()

main()
