#/bin/python3

def numero_Perfecto_while(num):
  var_aux = 1 
  suma = 0 
  while(var_aux < num):
    if num % var_aux == 0: 
      suma += var_aux # lo mismo suma = suma + var_aux 
    var_aux += 1 

  if suma == num:
    res = True 
  else:
    res = False

  return res 
# return suma == num 

def numero_Perfecto_for(num):
  suma = 0 

  for i in range(1,num):
    if num % i == 0:
      suma += i 

  if suma == num:
    res = True 
  else:
    res = False
  
  return res 

def primero_4_Perfectos_while():
  # break  --> no usar
  # continue 
  lista_numero = [] 
  # len() --> cantidad de elementos de la lista
  # lista_numero[]
  # listas empiezan de 0 a n [0]
  # lista[] = {1,2,3,4,5}
  #            0,1,2,3,4 
  # lista[2] = 3 
  # lista[1] = 2 
  var_aux = 2
  num_aux = 0

  # Agregar elementos en la lista :
  # append(elemento)
  # lista.append(elemento)

  # lista = { 1,2,3,4,}
  # lista[1:2]

#  2 , 3 , 4, 5 ,6,7 .... infinito  
  while(num_aux < 4):
    res = numero_Perfecto_for(var_aux)
    if res: 
      num_aux +=1 
      print("El {} numero perfecto es : {}\n".format(num_aux,var_aux))

      
    var_aux +=1

def main():
  primero_4_Perfectos_while()

main()


#  6 = 1 + 2 + 3 
#  10 = 1 2 5
