#/bin/python3 

#ejercico 4- con while 
def numero_Perfecto(num):
    var=1 #porque es un num entero positivo 
    suma =0 #generalmente se inicializa en 0  
    while (var < num):
        if num%var == 0:
            suma += var #(lo mimso que suma = suma + var)
        var += 1 #(va sumando de uno en uno)
		
    if suma == num:
        res= True
    else:
        res = False
    return res
	
def numero_4_Perfecto():
    var= 2 #porque tenemos que empezar en un numero perfecto y 1 nunca va a ser perfecto, el 2 es el primero 
    var1= 0
    num_aux =0 
    while(num_aux <4):
        res= numero_Perfecto(var)
        if res:
            num_aux += 1 # encontré un numero más 
            print ("El {}numero perfecto es:{} ".format(num_aux,var))
        var  +=1 
		
def main ():
    numero_4_Perfecto()
    print (numero_Perfecto(6))
    print (numero_Perfecto(4))
    print (numero_Perfecto(12))
main()
