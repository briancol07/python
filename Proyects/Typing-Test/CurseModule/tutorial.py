#!/bin/python3

# built in python
# in windows pip install windows-curses

import curses 
from curses import wrapper 

# define a main with a standard screen

def main(stdscr):
    # clear screen
    stdscr.clear()
    # write in screen
    stdscr.addstr("Hello world!")

    # to avoid it close (like getchar())
    stdscr.getkey()

    # to add color 
    # (Number,color-of-letter, background) 
    curses.init_pair(1,curses.COLOR_GREEN,curses.COLOR_BLACK ) 
    curses.init_pair(2,curses.COLOR_RED,curses.COLOR_BLACK) 
    curses.init_pair(3,curses.COLOR_WHITE,curses.COLOR_BLACK) 

    stdscr.addstr("Hello world!",curses.color_pair(1))
    stdscr.addstr("Hello world!",curses.color_pair(2))
     
    stdscr.refresh()
    stdscr.getkey()


wrapper(main)

