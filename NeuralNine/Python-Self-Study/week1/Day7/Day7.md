# Day 7 

## String Functions 

> Treat them as a sequence of characters 

* len(string)

## Character for: 

* \n 
* or """ """
* \t ---> tab
* \b ---> backspace 
* \s space
```python 

text = "hello world \n this is awsome" 

print(text)
```

## Formating 

* %s ---> string 
* %d ---> decimal 

* upper 
* lower 
* title
* swapcase
* count
  * count how many times the word/characters is in the string 
* find 
  * returns the index of the string (always the first)
* join
* split   
* replace

``` python 

name = "pepe"
age = 34

print("my name is %s and I am %d years old" %(name,age))
print("my name is {} and I am {}  years old".formar(name,age))
name = name.upper()
print(name)
name = name.lower()
print(name)

separator = ';'
mylist = ['kitchen','dog','mike']
print(separator.join(mylist))

text = "I am happy becuase my name is richard "

words = text.split('a')
print(words)
```


