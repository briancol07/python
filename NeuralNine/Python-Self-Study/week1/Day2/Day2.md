# Day 2 

## User input 

use input function 

```python
x = input("Enter number1")
y = input("Enter number 2")
# You can make a cast before

x = int(input())

```
## If statements 

use comparison , if its true do something else no 

basic structure

```python

if something:
  do this
else:
  do that 

#--------

if something:
  do this
elif other:
  do this
else:
  do this
```
you can add more ifs in between of each one

```python
if something:
  do this
  if somthing:
    do this
elif other:
  do this
  if somthing:
    do this
  else:
    do that
else:
  do this
  if somthing:
    do this
```

