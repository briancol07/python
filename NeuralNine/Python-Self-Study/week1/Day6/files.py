#!/bin/python

# Files 


try: 
    file = open('file.txt','r')
except:
    file = open('file.txt','a')
    file.write("Hello world")
    file.flush()
finally:
    file = open('file.txt','r')
    content = file.readline()
    print(content)
    file.close()

