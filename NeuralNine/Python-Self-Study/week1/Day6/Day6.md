# Day 6 

## Files 

### How to open file 

```python 

file = open('myfile.txt','r')

```

Open(nameFile,HowtoOpen)

* How to open
  * Write (w)
    * allways override the file
    * if dosen't exist it creates 
  * Read  (r) 
  * Append (a)

### How to close file 

```python
file.close()
```
Always you open a file you must close it, when you use this option it flush its content 
When you close it also call a method called flush, but if you don't close it will never write the things there 
### Alternative 

> this alternative do not need to close 

```python
with open('file.txt','r') as f:
  # All of my coe 
  pass
``` 
After the with the file close it

### Usually do

``` python
try:
  # some stream code
except: 
  #  catch
finally:
  # close 
```
-----

## Do more stuff

* delete
* create
* rename 

```python 

from os import * 

mkdir("test")
chdir("test")
mkdir("newdir")
rename("blabla","bla2")
remove("bla2")
```
