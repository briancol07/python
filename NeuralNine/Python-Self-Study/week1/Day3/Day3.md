# Day 3 

## While loop 

Executes a piece of code over and over until the condition return false 
while the condition is true make this .

```python
x = 0 
while x < 10:
  x +=1         # make the loop end wwhen x = 10  
  print(x)

while True:     # this is an infinite loop
  print('Something)
```

## For

For counting or telling python how many times 
for a value in this range do this
```python 

for x in range(10):
  print(x)
for x in range(1,21):
  print(x)

```
* To cut the loop:
  * break 
  * continue 
    * Don't cut it but it skip this iteration 
  * pass 
    * to continue an do not enter, when you dont know 

```python 
x = 0

while x < 10:
  if x == 5:
    break
  if x ==3:
    continue
  x += 1
  print(x)
```
## Sequence and Collections 

> Sequence is collection of elements 

### list 

can be more than 1 type of list 

```python

mylist = [10,2,30,344,2334]
print(mylist)
print(mylist[:3]) # print the list until the 3 element (not included)
print(mylist[1:3]) # print first until three but the first is included
mylist[3] = "my new string"
print(mylist)

```
you cant add something to a non existing position


```python
# This will print all the elments in the list with a jump of line  

mylist = [10,2,30,344,2334]
for x in mylist:
  print(x)
```

To join string (concatenate one next to the other

```python 
x = [1,2,3]
y = [4,5,6]
print(x + y)
# This will write 4 times list
print(x * 4)
```

### Some Functions

* len 
  * give the size of the string
* max 
  * give the biggest 
  * must be the same values
* min 
  * give the lowest 
  * must be the same values 
* append
  * put something at the end of the list 
* insert 
  * insert in a specific location
  * first postion later thing to insert
* remove 
  * remove only a value 
* pop
  * without paremeters it delete the last element
  * with parameter delete element in a specific position
* index
  * will tell the position of the element in the list
* sort 
  * it put it in order 

```python 
x = [1,2,3]
print(len(x))
print(max(x))
print(min(x))
x.append(4)
print(x) 
x.insert(2,5)
x.remove(2)
x.pop(2)
print(x.index(1))
x.sort()
print(x) 

```

### Tuples 

> you can not change size of tuple, you can modify but not change size

```
x = [1,2,3]
y = (1,2,3)
y[2] = 10

```
### Diccionaries 

key : value 

* items
* key
* values 

``` python

person = {'name': 'Mark', 'age':25,'gender': 'm'}
print(person)
print(person['age'])
print(person['name'])

# to add key
person['newkey'] = 345

##-------

print(person.items())
print(person.keys())
print(person.value())

```



