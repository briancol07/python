# Day 5 

There are different type of errors
* Zero division error
* cant convert in that data type 


```python

try: 
  x = int(input("first number")
  y = int(input("second number")
  print(x/y)
except:
  print("There was an error:")
``` 

instead of breaking the program if it cant make the division it goes to the execpt part 

see [Day5.py](./Day5.py)

```python 

def someFunction():
  if True:
    raises Exception("Something went wrong")

someFunction()
x = 100
y = 20 
assert(x<y) 
```

This to raise your own exception or some of them
also you can do assert


