#!/bin/python

try:
    x = int(input("First Number"))
    y = int(input("Second Number"))
    print(x/y)
except ValueError:
    print("Please enter a valid number next time")
except ZeroDivisionError:
    print("Cannot divide by zero!")
finally:
    print("Done")
# when we use streams we usually use try and except 



