# Day 1 

1. Print hello world 

```python 
print('Hello world')
```
function print , accept parameters between parenthesis, cuotation mark shows that is a text 

2. Variables and Data Type 
  * str ==> "string",'string',""" Multi line"""
  * int ==>0,9,-5 
  * float ==>0.9,9.0403,-0.2323
  * complex(10,9)
  * bool ==> True,False 
  * list,tuple,dict

string is diferent from int 
"10 + 10" = 1010
10 + 10 = 20 

With the function type you can know the type 

>  Dynamically typed language this meas that we don't have to define a specific data type for our variable. 

### TypeCasting 

one example is that with string we can do mathematical operations so, we must make a cast to int or float or other 

```python
x = '120'
x = int(x)

print(x/4)
``` 
3. Operators and User Input 

* + - * / 
* % module gives the reminder 
* \*\* Exponent operator 

```python 
x = 10
# Both do the same  
x = x + 10
x += 10 
```

### Comparison Operations 

> Gives true or false 

* >= grater equal than / > only greater 
* <= less equal than / < less than  
* != 
* == 

### Input

Most of the time save the value in a variable, its always string ,so if you want int you must do a cast

```python
x = input()
y = int(input('ingrese valor'))
```
