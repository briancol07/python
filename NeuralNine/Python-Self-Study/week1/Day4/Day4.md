# Day 4

> Block of reusable code 


```python
# def helloworld(parameters):
#   Statements

def helloworld():
  print("hello world")

helloworld()

def add(x,y):
  print(x+y)

add(18,1)

def add2(x,y=0):
  return x+y

add2(14,35)

def mysum(*numbers):
  result = 0
  for number in numbers:
    result += number
  return result

print(mysum(1,23,3,4,5))
``` 
In the add function we pass 2 parameters  x ,y and when we call it we must give them 

we can give a default value (to make them optional) , so its not necesary to give new values, 

the last one the * in the parameter it's mean that we gona pass n number of parameters
