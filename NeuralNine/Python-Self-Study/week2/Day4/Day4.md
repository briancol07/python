# Day 4

## Queues

> Similar to list  but have a specific order 

```python

import queue
```

to use data , for example an array and tell the other threads where you have to go to use that data 
we use like pop the element

see [Queues](./queues.py)

an example of queues [PortScanner](https://www.neuralnine.com/threaded-port-scanner-in-python/)

----

## Sockets 

> End point of the comunication (server & client)

1. TCP
2. UDP

see [Server](./server.py)
see [Client](./client.py)
