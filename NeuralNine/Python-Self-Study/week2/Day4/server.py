#!/bin/python

import socket 

#TCP connection oriented 
#UDP faster 

#ip - port

# TCP
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

#UDP ---> SOCK_DGRAM 
# The local ip , port
s.bind(('127.0.0.1',55555))
# Will wait until someone connect
s.listen()

while True:
    # s.accept() accept any client 
    client,address = s.accept()
    print("Conneted to {}".format(address))
    # with encode utf-8 default
    client.send("You are Conneted".encode())
    client.close()



