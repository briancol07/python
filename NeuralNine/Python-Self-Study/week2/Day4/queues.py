#!/bin/python

import queue 

#FiFO 
q = queue.Queue()
numbers = [10,20,30,40,50,60,70]

for number in numbers:
    # this will put the elementes queue
    q.put(number)
# Get the next element 
print(q.get())
print(q.get())

# LIFO
y = queue.LifoQueue()
nums = [1,2,3,4,5,6,7]
for x in nums:
    y.put(x)

print(y.get())
print(y.get())

# Sorted by priority 

x = queue.PriorityQueue()

x.put((2,"Hello world!"))
x.put((11,99))
x.put((5,7.5))
x.put((1,True))

while not x.empty():
    print(x.get())

