#!/bin/python

## Our first class 

class Person:

    # class variable
    test = 0
    def __init__(self,name,age,height):
        self.name = name
        self.age = age 
        self.height = height 
        Person.test +=1
    def __del__(self):
        Person.test -=1
        print("object deleted")

    def __str__(self):
        return "Name:{} , Age: {}, Height: {}".format(self.name,self.age,self.height)

person1 = Person("Mike",30,180)
person2 = Person("aaa",20,10)
print(person1.name)
print(person1.age)
print(person1.height)
person1.name = "Henry"
print(person1.name)
del person1
print(person2) 
print(Person.test)
