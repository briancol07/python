#!/bin/bash


class Person:

    def __init__(self,name,age,height):
        self.name = name
        self.age = age
        self.height = height
    def __str__(self):
        return "Name {}, age {},Height:{}".format(self.name,self.age,self.height)
    def get_older(years):
        self.age += years

class Worker(Person):
    def __init__(self,name,age,height,salary):
        # here the super calls the parent function 
        super(Worker,self).__init__(name,age,height)
    def __str__(self):
        # Here we call the function from the parent and later we override 
        text = super(Worker,self).__str__()
        text += ", Salary {}".format(self.salary)
        return text
    def cal_yearly_salary(self):
        return self.salary *12

worker1 = Worker("henry",32,130,30000)
print(worker())
print(worker1.cal_yearly_salary())
