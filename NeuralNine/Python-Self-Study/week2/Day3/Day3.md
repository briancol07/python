# Day 3 

## Recursion 

> To create a recursive function that calls itself.

![recurse](../img/recurse.png)

see [factorial](./factorial.py)


```
factorial(3)          # 1st call with 3
3 * factorial(2)      # 2nd call with 2
3 * 2 * factorial(1)  # 3rd call with 1
3 * 2 * 1             # return from 3rd call as number=1
3 * 2                 # return from 2nd call
6                     # return from 1st call

```

![factorial](../img/factorial.png)

> Every recursive function must have a base condition that stops the recursion or else function call itself infinitely. 

By default, the maximum depth of recursion is 1000.if the limit is crossed, it results in RecursionError. 

```
def recursor():
  recursor()

recursor()
```

* Advantage of Recursion
  1. Recursive Functions make the code look clean and elegant
  2. Acomplex tsk can be brken down into simpler sub-problems using recursion
  3. Sequence generation is easier with recursion than using some nested iteration
* Disadvantages of Recursion
  1. Sometimes the logic behind recursion is hard to follow through
  2. Recursive calls are expensive(inefficient) as the take up alot of memory and time
  3. Recursive functions are hard to debug.

```python
def fibonacci(n):
  a, b = 0, 1 
  for x in range(n-1):
    a, b = b , a+b
  return a

def fibonacci2(n):
  if n <=1:
    return n
  else:
    return (fibonacci2(n-1) + fibonacci2(n-2))


print(fibonacci(3))
print(fibonacci2(3))
```
## Threading 

> It allow speed up program executing multiple task at the same time

```python
import threading 

# define a function

t1 = threading.Thread(target=function)
t1.start()
```

the first line of t1 calls the thread, pointing to the target that is the function, we don't use function() because we don't want to start inmediately.
With t1.start() , run the function

it will be usefull with videogames 

see [threading](./threading.py)

```python

t3.join()
print("hello")
```
this make the main program to wait until the t3 ends. 


