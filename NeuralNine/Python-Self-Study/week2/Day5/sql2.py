#!/bin/python3

import sqlite3 

connection = sqlite3.connect('mydata.db')
cursor = connection.cursor()

cursor.execute(""" 
        CREATE TABLE IF NOT EXISTS persons(
            first_name TEXT,
            last_name TEXT,
            age INTERGER
        )
""")

cursor.execute(""" 
        INSERT INTO persons VALUES
            ('Paul','Smith',24),
            ('Mark','johnson',25)
            """)
cursor.execute("""
        SELECT * FROM persons
""")
rows = cursor.fetchall()
print(rows)

connection.commit()
connection.close() 
