#!/bin/python3

import sqlite3 

# Connection ---> local database--> if exist it will connect 

connection = sqlite3.connect('mydata.db')#!/bin/python3

cursor = connection.cursor()

# create table 

cursor.execute("""
CREATE TABLE persons(
    first_name TEXT,
    last_name TEXT,
    age INTEGER)
""")
# to see the changes in the database 
connection.commit()
# Close connection with the database 
connection.close()


