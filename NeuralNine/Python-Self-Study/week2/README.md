# Week 2 

## Intermediate lvl 


| Day | Concept | Summary |
| ------ | ------ | ------ |
| 1 | Classes and Objects (OOP) | Learn the basics of object-oriented programming. |
| 2 | Inheritance | Learn about parent and child classes. |
| 3 | Recursion | Write recursive functions instead of iterative functions. |
| 3 | Multithreading | Run multiple threads in the same Python script. |
| 4 | Queues | Use the queue data structure to control the access to a ressource. |
| 4 | Sockets | Learn the basics of network programming in Python. |
| 5 | Database Programming | Execute basic SQLite commands on a database from a Python script. |
| 6 | Logging | Learn to log messages using different security levels. |
| 7 | XML & JSON | Learn to process and create XML and JSON files. |
| Opt. | Regular Expressions | Filter and change texts using regular expressions. |

