# Week 3 

## Advanced lvl 

| Day | Concept | Summary |
| ------ | ------ | ------ |
| 1 | List Comprehensions | Create Python lists in a functional way. |
| 1 | Mapping & Filtering | More functional aspects of Python. |
| 1 | Lambda Expressions | Learn to write lambda expressions in Python. |
| 2 | Docstrings | Learn proper Python documentation. |
| 2 | Magic Methods | Learn how to use magic methods or dunders. |
| 3 | Decorators | Wrap additional code around functions using decorators. |
| 4 | Generators | Learn how to yield results instead of returning them. |
| 5 | Argument Parsing | Parse arguments from the command line. |
| 5 | Encapsulation | Learn about data hiding and data abstraction. |
| 6 | Type Hinting | Learn how to hint Python types for a more static approach. |
| 7 | Design Patterns | Learn about different design patterns (Factory, Proxy...) |
