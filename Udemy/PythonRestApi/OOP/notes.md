# Notes 


how to change atributes

its simple like

``` python

  bls.course = "html"
```

also yoy can delete atributes with the key word **del** 

``` python

del bls.course
```

## Class Vs Instance (object) variables

Class Variables | Instance (object) Variables
----------------|-----------------------------
Defined outside any method | Defined inside class methods 
Accessed outside class with className | Accessed outside class with objectName
Not prefixed | Prefixed with self keyword 
Modification affects all class instances | Modification to object is local 
Class are indented | Instances are not indented 

## Inheritance  

The super class / parent 

> The process of creatin new classes and reusing methods and attributes from the parent class.

Parent --> Child 

If you create a child class without an init() method it inherits all method and attributes of the parent class 
If you create the init method in the child you will override it in your new class 


``` python 
# to use the same variables of the father method

class Lawyers(Person):
    def __init__(self,fname,lname):
        Person.__init__(self,fname,lname)
#        self.firstname = fname
#        self.lastname = lname
        
    def printinfo(self):
        print(self.firstname,self.lastname)

```


## Polymorphism

The ability to take or have various forms 

Polymorphism allows us to define methods in child class with same name as methods in parent class buy doing different things

## Encapsulation 

Process of restricting access to methods and variables to preven direct data modification

Public methods and variables are accessible from anywhere in program 

Private method and variables are accessibles from their own class

To make the variable private in python you will do doble underscore  
 
To change the values and to see it if they are private you need the setter or getter

## Abstraction 

Abstraction hides the internal details and shows only functionalities.

### Abstract Classes 

They are classes that contain one or more abstract methods
They can not be instantiated 
They require subclasses tyo provide implementation for the abstract methods

### Abstract methods 

They are methods that are declared but contains no implementation

python have a module call ABC Abstract Base Classes 

A decorator allows you to add new functionality to an existing object ( classes, methods, functions) without modifying its structure 

## Modules 

A python module is a python file that can contain:

* Functions
* Global variables 
* Other objects

### How to use module 

use the module in any python ,by importing 

``` python

import module_name
from module_name import (whatyouneed) 


```

### built-in Modules

These are modules python has already created  

* random 
* platform
  * to tell the platform that I am using

## Packages 

Packages are just folders that contains modules 
They contain a special file named \_\_init\_\_.py
the \_\_init\_\_.py file can be empty. The file tells python that the directory of folder contains a python package which can be imported like a module 
Package are a convenient way to organize modules 

### Accessing module in package _

with the function dir you can see all the objects in the package

## Pycache (.pyc)

This is a folder containing python3 bytecode compiled and ready to be executed 
They make your programs start faster
They are automatically created after a module has been imported and used 
They are created in the same directory as the modules

## python name attribute

* Every module in python has a special variable called name
* The value of the name attribute is set to main when the module is executed as the main program 
* The value of the name can be set to another module if imorted or module it is called from 
* The if statement prevents certain code within the block from being executed

``` python 
if __name__ = "__main__"
```
allows you to run python files as reusable modules or standalone programs 


