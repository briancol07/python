#!/bin/python3 

class Cars:
    def __init__(self,speed,color):

        self.__speed = speed
        self.__color = color 
    # This are not private variables  
       # self.speed = speed
       # self.color = color 
        

    def set_speed(self,value):
        self.__speed = value 

    def get_speed(self):
        return self.__speed


ford = Cars(250,"green")
nissan = Cars(300,"red")
toyota = Cars(200,"blue")

ford.set_speed(450)
# Because there are no encapsulation you can change in this way 
# If the variables are private you cant change in this way
# ford.speed = 500


print(ford.get_speed())
#print(ford.color)

