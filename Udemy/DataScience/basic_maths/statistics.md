# Let's Start with Statistics 

## Intro 

> What is Statistics ? 

Statistics is the science of conductiong studies to collect, organize, summarize, analyze and draw conclusions from the data 

## 10 uses of Statistics 

![10-uses](./img/10-uses.png)

## What is Data ?

> a data is a collection of raw information such as facts, numbers, words, measurements, obsevations or even just descriptions of things 

### Types of data 

1. Primary Data 
  * Web analytics 
  * Surveys 
  * Listenings Labs 
2. Secondary Data
  * Published Articles 
  * Blogs 
  * White Papers 

### Primary Data 

> Data that has been collected first hand by the researcher for addressing the population at hand 
