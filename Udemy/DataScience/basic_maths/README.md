# Basic Maths 

1. Statistics 
2. Data Quality
3. Types of Statistics 
4. Measures of Spread 
5. Measures of Shapes 
6. Plots Visualisation
7. Inferential Statistics 
8. Probability 
9. Conditional Probability 
10. Random Variables 
11. Normal Probability distribution
12. Central Limit Theorem 
13. Hypothesis Testing for Decision Making
