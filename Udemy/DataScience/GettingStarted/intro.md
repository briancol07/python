# Introduction 

## 1. What is data? 

Data are raw ingredients from which statistics are created, from the statistic you obtain information

## 2. Why Data science?

* Growin Demand 
* Easy to grab a job
* Unbeaten salaries
* Evolving field
* Add value to business

## 3. Who is data Scientist

A good data scientist is someone who can ask good questions 

* What
* Why 
* How 

Also you need to be :

* Business Expert 
* Innovator 
* Solution Architect 
* Network builder 


## 4. The lifecycle of data Science 

1. Business Understanding 
  * Ask relevant question and define obejtives for the problem
2. Data Mining 
  * Gather and scrape the data necessary for the proyect 
3. Data Cleaning
  * Fix the inconsistencies within the data and handle the missing values
4. Data Exploration
  * From hypothesis about your defined problem by visually analysing the data 
5. Feature Engineering 
  * Select important feature and conduct more meaningful ones using the raw data
6. Predictive Modeling 
  * Train machine learning models, evaluate their performanceand use them to make predicctions
7. Data Visualization 
  * Communicate the findings with they stakeholder using plots and interactive visualizations 

Where the information comes from 
What ir represents
How it can be turned into a valuable resource 
        
## 5. Jobs Roles

### Data Analyst

* Responsible:
  * Data Visualization
  * Processing masive amount of data 

### Data Engineer

It engineer the building, scalable big desing ecosystems for the businessses so that the lead scientist 

### Database Administrator 

They're responsible for the proper functioning of all the databases and of any enterprise and grand 

### Machine learning engineer

They are very highly in-demand, your profile comer with its challenges. They are also expected to perform a distinct and build pipelines.

### Data Scientist 

They have to understand the challenge of business and also best solutions using data analysus and data processing.

### Data Architect 

The architect creates a blueprint for data management so that the databases can be easily integrated, centralised and protected.

### Statistician 

Has a solid understanding of statistical theories and data organizations. They have to create a new methodologies for data engineers to apply 

### Business Analyst 

Have a good understanding of how need oriented technologies work and how to handle large volume of data.
Understanding or the separate high value data and low value data.

## 6. Data Science Component 

1. Data Strategy 
2. Data Engineering 
3. Data Analysis & Models 
4. Data Visualizatino & Operationalization 

### 1. Data Strategy 

They are looking at that strategy is simply did the mining one day? Are you going to gather and why it requires you to make the connection between the data you are going to gather and your business go. 

### 2. Data Engineering 

It is about technology and systems, the deliveries to access and organize the use of data.Creating the pipeline and end points within the system 

### 3. Data Analysis & Models 
 
This is the heart of data science. It's where a lot of what we associate with data science happens computing, math , statistics , domain. 
It uses the be by performing sidebars like to describe extract insights or to make predictions about a service product, person business or more likely, a combination of them 

### 4. Data Visualizatino & Operationalization 

It's not just about talking the data analysis and presenting correctly. Sometimes it involves going back to the raw data and understanding what need to be visualized based.
Operationalisation operationalizing is really about doing something with the leader
