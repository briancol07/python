# Python Developer:

* Python
* Django 
* Flask
* PostgreSQL
* MySQL
* API  

## What you should know ? 

* Basic Understanding of database concepts
* Basic use of SSMS (SQL Server Management Studio)
* Basic SQL or T-SQL knowledge (microsoft version)

## Who should take this course ?

* Beginner Database Administrators 
* Data scientist
* Data Analyst 
* Aspiring Big Data Professional 
* Anyone interested in Data

## What you will learn ? 

* How to design a data warehouse 
* How to implement a data warehouse
* Logical Data warehouse design
* Physical data warehouse design 
* Create ETL solution 
* Implement ETL process
* Big data concepts
* Google's big data approach
* MapReduce
* Hadoop
* High volume data
* High velocity and variety data 


