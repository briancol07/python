# High level VS Low Level Programming languages 


## High-level programming languages 

* These are languages only human can understand 
* They need an interpreter or complier for the machine to understand them 
* Examples:
  * Python
  * Java
  * C#
  * C++
  * etc 

## Low-level Programming languages

* These are languages only machines can understand 
* They do not need an interpreter or compiler 
* They are closer to computer hardware 

![highvslow](../img/highvslow.png)


High Level | Low Level
-----------:----------
Easy to learn | Difficult to learn 
Near to human | Near to machine 
Translator is required | No Translator
Low in execution | Fast In execution 


