#!/bin/python3 

# Imprimir numeros de 5 a 1 de manera descendente usando funciones recursivas. Puede ser cualquier valor positivo, ejemplo, si pasamos el valor de 5, debe imprimir:


def imprimir(numero):
    if numero > 0 :
        print(numero)
        imprimir(numero -1 )

imprimir(5)
