#!/bin/python3 

# Funcion con cantidad de valores no definidos 
# Esta funcion a todos los argumentos los trata como una tupla 

def suma(*args):
    resultado = 0
    for x in args:
        resultado += x
    return resultado

# Funcion con cantidad de parametros indefinidos pero son diccionarios 

def mostrarDiccionario(**args):
    for llave , valor in terminos.items():
        print(f'{llave}: {valor}')

# Funciones Recursivas 

def factorial(numero):
    if numero ==1:
        return 1 
    else:
        return numero * factorial(numero -1)
    
