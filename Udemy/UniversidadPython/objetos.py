#!/bin/python3

# Clases se nombran con mayuscula

## init es un metodo constructor 
# Es para declarar los valores iniciales 
# Double underscore = dunder

class Persona:
    def __init__(self):
        self.nombre = 'juan'
        self.apellido = 'perez'
        self.edad = 29

class Persona2:
    def __init__(self,nombre,apellido,edad):
        self.nombre = nombre
        self.apellido = apellido
        self.edad = edad 







# Se esta creando la instacia que llama al constructor 

persona1 = Persona()
persona2 = Persona2("pepe","alvarez",32)
print("persona1")
print(persona1.nombre)
print(persona1.edad)
print(persona1.apellido)
print("persona2")
print(persona2.nombre)
print(persona2.edad)
print(persona2.apellido)

print(type(Persona))
