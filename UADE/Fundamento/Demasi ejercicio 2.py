"""Ejercicio 2

Ingresar por teclado la cantidad de términos a generar de la siguiente serie:

1 7 19 43 91 187 379 763 1531 3067 6139

El primer término es el 1 y cada término se genera como el doble del
término anterior más 5.

Mostrar la serie por pantalla e informar la suma de los términos generados."""


num=int(input("cuantos terminos desea generar?:"))
termino=1
print(1,end=" ")
anterior=1
suma=1
while num>1:
    termino=anterior*2+5
    print(termino,end=" ")
    suma=suma+termino
    anterior=termino
    num=num-1
print("Se obtuvo",suma,"sumando todos los numeros generados")
    

    
