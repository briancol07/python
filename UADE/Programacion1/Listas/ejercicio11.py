#!/bin/python3 

# Resolver el siguiente problema , diseniando las funciones a utilizar:

# Una clinica necesita un programa para atender a sus pacientes.Cada paciente que ingresa se anuncia en la recepcion indicando su numero de afiliado(numero entero de 4 digitos) y ademas indica si viene por una urgencia (ingresando un 0) o con turno (ingresando un 1). Para finalizar se ingresa -1 como numero de socio. 

# Se solicita
#   a . Mostrar un listado de los pacientes atendidos por urgencia y un listado de los pacientes atendidos por turno en el orden que llegaron a la clinica 
#   b. Realizar la busqueda de un numero de afiliado e informar cuantes veces fue atendido por turno y cuantas por urgencia. Repetir esta busqueda hasta que se ingrese -1 como numero de afiliado 

# Hay que testear algunos casos bien !!! 
# ----------------------  A ----------------------------------------------------
def listaUrgencia(pacientes,asistencia):
    urgencia = [int(pacientes[x]) for x in range(len(asistencia)) if asistencia[x] == 0]
    return  urgencia

def listaConTurno(pacientes,asistencia):
    turno  = [int(pacientes[x]) for x in range(len(asistencia)) if asistencia[x] == 1]
    return  turno

# ----------------------  B ----------------------------------------------------

def busquedaAfiliado(paciente,asistencia): 
    nroAfiliado = input("ingrese Numero de afiliado")
    while int(nroAfiliado)!= -1:
        countUrgencia = 0
        countTurno = 0 
        while len(nroAfiliado) !=4 :
            nroAfiliado = input("Ingrese su numero de paciente")
        indicesAfiliado = [index for index in range(len(paciente))]  
        for x in indicesAfiliado:
            if asistencia[x] == 0:
                countUrgencia += 1
            elif asistencia[x] == 1:
                countTurno += 1
            else:
                continue

        print(indicesAfiliado)
        print("Urgencia =  ",countUrgencia )
        print("Turno  =  ",countTurno )
        nroAfiliado = input("ingrese Numero de afiliado")



# ------------------- Main ----------------------------------------------------
paciente = []
asistencia = []

nroPaciente = input("Ingrese su numero de paciente")
while int(nroPaciente) != -1: 
    while len(nroPaciente) !=4 :
        nroPaciente = input("Ingrese su numero de paciente")

    paciente.append(nroPaciente)
    
    tipoAsistencia = int(input("Ingrese como va a ser atendido"))
    while tipoAsistencia != 0 and tipoAsistencia !=1:
        tipoAsistencia = int(input("Ingrese como va a ser atendido"))
    asistencia.append(tipoAsistencia)

    nroPaciente = input("Ingrese su numero de paciente")

print(paciente)
print(asistencia) 
print(listaUrgencia(paciente,asistencia))
print(listaConTurno(paciente,asistencia))
busquedaAfiliado(paciente,asistencia)
