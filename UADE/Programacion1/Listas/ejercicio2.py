#!/bin/python3

# Escribir funciones para:

# a. Generar una lista de 50 numeros aleatorios del 1 al 100
# b. Recibir una lista como parametro y devolver True si la misma contiene algun elemento repetido. La funcion no debe modificar la lista 
# c. Recibir una lista como parametro y devolver un nueva lista con los elementos unicos de la lista original, sin importar el orden.

import random 


#------------A------------------------ 

def gen50List():
    lista = [ ]
    for i in range(50):
        lista.append(random.randint(1,100))
    return lista

#------------B------------------------ 
def hayRepetidos(lista):
    for x in range(len(lista)):
        for i in range(len(lista)):
            if (i != x) and (lista[i] == lista[x]):
              #  print(lista[i]) # Esto es para que me muestre el repetido 
                return True 
    return False 

#------------C------------------------ 

def unicos(lista):
    unicosLista = []
    flag = True 
    for x in range(len(lista)):
        for i in range(len(lista)):
            if (i != x) and (lista[i] == lista[x]):
                flag = False
        if flag :
            unicosLista.append(lista[x])
            flag = True 

    return unicosLista



#------------ Main Program -------------                
print(gen50List())
print(hayRepetidos(gen50List()))
print(hayRepetidos([1,2,3,4,5]))
print(hayRepetidos([1,2,3,4,4]))
print(unicos([1,2,3,4,5]))
print(unicos([1,2,3,4,4]))

