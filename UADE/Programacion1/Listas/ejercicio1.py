#!/bin/python3

# 1 . Desarrollar cada una de las siguientes funciones y escribir un programa que permita verificar su funcionamiento imprimiendo la lista luego de invocar a cada funcion

# a. Cargar una lista con numeros al azar de 4 digitos. la cantidad de elementos tambien sera un numero al azar de dos digitos 
# b. Calcular  devolver la sumatoria de todos los elementos de la lista anterior 
# c. Eliminar todas las apariciones de un valor en la lista anterior. El valor a eliminar se ingrea desde el teclado y la funcion lo recibe como parametro.
# d. Determinar si el contenido de una lista cualquiera es capicua,sin usar listas auxiliares. Un ejemplo de listas capicua es [ 50, 17,91,17,50]

# capicua : se refiere a cualquier número que se lee igual de izquierda a derecha que de derecha
#---------  a  -----------------
# Se puede hacer de dos maneras una con random de 1000 a 9999 y la otra es hacerlo como string para que entre desde el 0000 hasta 9999 
# Haremos la primera por ser mas simple 

import random 

def cargarListaRandom():
    lista = [] 
    tama = random.randint(0, 10)
    for i in range(tama):
        lista.append(random.randint(1000,9999))
    return lista 

def sumListaRandom(lista):
    suma = 0 
    for x in lista:
        suma += x
    return suma 

def eliminarElemLista(elem,lista):
    print(lista) # Esto es solo para ver la lista 
    tama = len(lista)
    for x in range(tama): 
        if elem == lista[x]:
            lista.pop(x)
    return lista

def listaCapicua(lista):
    print(lista[::-1])
    print(lista)
    if lista == lista[::-1]:
        return True
    return False

# Lo que hago siempre es mandarle la lista random aun que se puede probar con otras listas 

print(cargarListaRandom())
print(sumListaRandom(cargarListaRandom()))
eliminar = int(input("Ingrese el elemento a sacar de la lista"))
print(eliminarElemLista(eliminar,cargarListaRandom()))
print(listaCapicua(cargarListaRandom()))
print(listaCapicua([1,2,3,4,5]))

