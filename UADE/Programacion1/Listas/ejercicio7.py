#!/bin/python3

# Intercalar los elementos de una lista entre los elementos de otra. La intercalacion debera realizarse exclusivamente mediante la tecnica de rebanadas y no se creara una lista nueva sino que se modificara la primera. Por ejemplo, si lista1 = [8,1,3] y lista2 = [5,9,7] , lista1 debera quedar como [8,5,1,9,3,7]

def apareo(lista1,lista2):
    y = 1
    for x in range(len(lista2)):
        lista1[x+y:x+y] = [lista2[x]]
        y += 1 
    return lista1


lista1 = [8,1,3]
lista2 = [5,9,7]
print(lista1)
print(lista2)
print(apareo(lista1,lista2))
print(apareo([1,2,3,4],[5,6,7,8,9]))
