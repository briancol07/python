#!/bin/python3 

# Generar una lista con numeros al azar entre 1 y 100 y crear una nueva lista con los elementos de la primera que sean impares. El proceso debera realizarse utilando lsitas por comprension. Imprimir las dos listas por pantalla 

import random 

def listaAzar():
    lista = [random.randint(1,100) for elem in range(1,20)]
    return lista 

def listaImpares(lista):
    lista1 = [elem for elem in lista if elem%2 != 0]
    return lista1


#----------------------- Main -----------------------------


azar = listaAzar()
print(azar)
print(listaImpares(azar))




