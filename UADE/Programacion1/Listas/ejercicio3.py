#!/bin/python3

# Crear una lista con los cuadrados de los numeros entre 1 y N (ambos incluidos), donde N se ingresa desde el teclado. Luego se solicita imprimir los ultimos 10 valores de la lista. 

def listaCuadrados(n):
    lista = []
    for i in range(1,n+1):
        lista.append(i**2)
    if len(lista) >= 10:        
        return lista[:-11:-1]
    else:
        return lista[::-1]

def listaCuadradosOriginal(n):
    lista = []
    for i in range(1,n+1):
        lista.append(i**2)
        
    return lista


#----------MAIN------------------------
num = int(input("Ingrese un numero "))
print(listaCuadradosOriginal(num))
print(listaCuadrados(num))

#print(listaCuadradosOriginal(14))
#print(listaCuadrados(14))
