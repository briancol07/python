#!/bin/python3 

# Escribir una funcion que reciba una lista como parametro y devuelve True si la lista esta ordenada en forma ascendente o False en caso contrario. Por ejemplo, ordenada([1,2,3]) retorna True y ordenada(['b','a']) retorna False. Desarrollar ademas un programa para verificar el comportamiento de la funcion.

def chequearOrden(lista):
    # Uso la funcion sorted que nos devuelve la lista ordenada (se podria hacer con una funcion propia)
    if lista == sorted(lista):
        return True
    return False


#------------ Main ---------------

lista1 = [1,2,4,5,6,7]
lista2 = [3,2,1]
lista3 = ['b','c']
lista4 = ['z','x','a']

print(chequearOrden(lista1))
print(chequearOrden(lista2))
print(chequearOrden(lista3))
print(chequearOrden(lista4))
