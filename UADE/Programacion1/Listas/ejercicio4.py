#!/bin/python3

# Eliminar de una lista de palabras las palabras que se encuentre en una segunda lista.  Imprimir la lista original, la lista de palabras a eliminar y la lista resultante.


def eliminarPalabras(lista1,lista2):
    for x in lista2:
        item = x
        while item in lista1:
            lista1.remove(item)
    return lista1






listaA = ['pepe','lala','churro','juan','pedro','carlos','ricardo']
listaB = ['lala','carlos','pepe']

print(listaA)
print(listaB)
print(eliminarPalabras(listaA,listaB))


