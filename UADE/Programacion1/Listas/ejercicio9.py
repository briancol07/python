#!/bin/python3

# Generar e imprimir una lista por comprension entre A y B con los multiplos de 7 que no sean multiplos de 5 , A y b se debe ingresar desde el teclado 


def listaCompresionAB(a,b):
    lista = [ elem for elem in range(a,b) if elem %7 == 0 and elem % 5 !=0]
    return lista


#print(listaCompresionAB(0,10))

numA = int(input("Ingrese el numero A"))
numB = int(input("Ingrese el numero B"))
print(listaCompresionAB(numA,numB))


