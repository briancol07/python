#!/bin/python

# Se desea llevar un registro de los socios que visitan un club cada dia. Para ello se ingresa el numero de socio de cinco digitos hasta ingresar un cero como fin de carga

# Se solicita :
# A. Informar para cada socio,cuantas veces ingreso al club (cada socio debe aparecer una sola vez en el informe)
# B. Solicitar un numero de socio que se dio de baja del club y eliminar todos sus ingresos. Mostrar los registros de entrada al club antes y despues de eliminarlo . Informar cuantos registros se eliminaron 

# Falta Testearlo rigurosamente 

def socioEsta(listaSocios,socio):
    return socio in listaSocios 

#---------------------------  B   ---------------------------------------------

def eliminarSocio(listaSocios,cuentaEntrada):
    registrosEliminados = 0
    nroSocio = input("Ingrese su numero de socio")
    while int(nroSocio) != -1: 
        while len(nroSocio) !=5:
            nroSocio = input("Ingrese su numero de socio Acorde")
        
       # cuentaEntrada[listaSocios.index(nroSocio)] += 1 
       # Si queremos eliminar por index podemos usar del 
       # a = a[:index] + a[index+1 :] esto es con slices 
       # or the classic pop(index) 
        print("Lista antes", listaSocios)
        print("Ingresos antes",cuentaEntrada)
        cuentaEntrada.pop(listaSocios.index(nroSocio))
        listaSocios.remove(nroSocio)
        print("-----------------------------")
        print("Lista Nueva", listaSocios)
        print("Ingresos Nuevos", cuentaEntrada)
        registrosEliminados +=1 
        nroSocio = input("Ingrese su numero de socio") 
        return registrosEliminados  
        

#--------------------------- Main ---------------------------------------------
listaSocios = []
cuentaEntrada = []
nroSocio = input("Ingrese su numero de socio")
while int(nroSocio) != -1: 
    while len(nroSocio) !=5:
        nroSocio = input("Ingrese su numero de socio")
    if socioEsta(listaSocios,nroSocio):
        cuentaEntrada[listaSocios.index(nroSocio)] += 1 
    else:  
        listaSocios.append(nroSocio)
        cuentaEntrada.append(1)
    nroSocio = input("Ingrese su numero de socio") 

print(listaSocios)
print(cuentaEntrada)
print(eliminarSocio(listaSocios,cuentaEntrada))
