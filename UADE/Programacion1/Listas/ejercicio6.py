#!/bin/python3 

# Escribir una funcion que reciba una lista de numeros enteros como parametro y la normalice, es decir que todos sus elementos deben sumar 1.0, respetando las proporciones relativas que cada elemento tiene en la lista original. Desarollar tambien un programa que permita verificar el comportamiento de la funcion. Por ej: normalizar ([1,1,2]) debe devolver [0.25,0.25,0.50]

def normalizar(lista):
    # la funcion sum me devuelve la suma de todos los elementos
    suma = sum(lista)
    # Por comprension
    #normalizada = [number / suma for number in lista]
    # Recorriendo el bucle 
    normalizada = []
    for number in lista:
            normalizada.append(number / suma)
    return normalizada 



print(normalizar([1,1,2]))




