#!/bin/python3 

# Utilizar la tecnica de listas por comprension para construir una lista con todos los numeros impares comprendidos entre 100 y 200 

def comprension():
    lista = [ elem for elem in range(100,200) if elem%2 == 1 ] 
    return lista


print(comprension())


