#!/bin/python3

# Un productor frutihorticola desea contabilizar sus cajones de naranjas segun el peso para poder cargar el camio de reparto. La empresa cuenta con N camiones, y cada uno puede transportas hasta media tonelada (500 kilogramos). En un cajon caben 100 naranjas con un peso entre 200 y 300 gramos cada una. Si el peso de alguna naranja se encuentra fuera del rango indicado, se clasifica para  procesar ecomo jugo. Se solicita Desarrollar un programa para ingresar la cantidad de naranjas cosechadas e informar cuantos cajones se pueden llenar, cuantas naranjas son para jugo y si hay algun sobrante de naranjas que deba considerarse para el siguiente reparto. Simular el peso de cada unidad generando un numero entero al azar entre 150 y 350. 

# Ademas, se desea saber cuantos caminoes se necesitan para transportar la cosecha, considerando que la ocupacio del camion no debe ser inferior al 80%; en caso contrario el camion no sera despachado por su alto costo.

import random

# camiones ---> 1/2 tonelada 
# cajon ---> 100 naranjas ---> 200 a 300 gramos c/u 
# si supera eso va para jugo 
# indicar cuantos cajones puede llenar 
# peso de cada unidad random entre 150 y 350 
# random.randint(start, stop) 

cosecha = int(input("ingrese la cantidad de naranjas cosechadas"))
peso = 0
jugo = 0
aux = 0 
cajones = [0] 
contador = 0 
for i in range(0,cosecha):
    peso = random.randint(150,350)
    if peso < 200 or peso > 300:
        jugo += 1 
    else:
        if aux == 100:
            aux = 0
            contador += 1 
            cajones.append(0)
        aux += 1 
        cajones[contador] += peso 
        
print("tengo", len(cajones)," cajones")
print("peso por cajon")
for j in cajones:
    print(j,end=',')
print("\n")
print("Para jugo son: ",jugo)

# Se podria hacer de manera optimizada viendo cual de paquetes sumados a otros te da mas cerca de la cantidad 
# aca se hara de manera mas simple, sumara y amedida que se acerque y cumpla con el 80% estamos bien 


