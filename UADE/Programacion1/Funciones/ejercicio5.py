#!/bin/python3

# Escribir dos funciones separadas para imprimir por pantalla los siguientes patrones de asteriscos donde la cantidad de filas se recibe como parametro.

# ********      **
# ********      ****
# ********      *******
# ********      ********* 

# Los cuadrados son n Filas y n *2 columnas
# El triangulo es cada 2 por n filas 


def piramide(numFilas):
    for x in range(0,numFilas+1):
        print("*" * x * 2)

def cuadrado(numFilas):
    for x in range(0,numFilas):
        print("*" * numFilas  * 2)

num = int(input("ingrese el numero de filas"))

piramide(num)
print("\n")
cuadrado(num)
