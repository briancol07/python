#!/bin/python3 

# Desarrollar una funcion que reciba tres numeros enteros positivos y verifique si corresponden a un fecha valida (dia,mes,anio)Devolver True o False segun la fecha sea correcta o no. Realizar tambien un programa para verificar el comportamiento de la funcion 

def fechaEsCorrecta(dia,mes,anio):
    if anio > 0 and anio <2022:
        if mes > 0 and mes <= 12:
            if dia > 0 and dia <= 31:
                return True 
    return False 


num1 = int(input("ingrese el dia"))
num2 = int(input("ingrese el mes"))
num3 = int(input("ingrese el anio"))

if (fechaEsCorrecta(num1,num2,num3)) :
    print("La fecha es correcta")
else:
    print("La fecha es incorrecta")

