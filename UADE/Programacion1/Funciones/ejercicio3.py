#!/bin/python3

# Una persona desea llevar el control de los gastos realizados al viajar en el subterraneo dentro de un mes. Sabiendo que dicho medio de transporte utiliza es un esquema de taridas decrecientes ( detalladas en la tabla de abajo) se solicita desarrollar una funcion que reciba como parametro la cantidad de viajes realizados en un determinado mes y devuelva el total gastado en viajes. Realizar tambien un programa para verificar el comportamiento de la funcion

# Cantidad de viajes | Valor del pasaje
# 1 a 20             | Averiguarl valor Actualizado
# 21 a 30            | 20% de descuento sobre tarifa maxima
# 31 a 40            | 30% de descuento sobre tarifa maxima 
# mas de 40          | 40% de descuento sobre tarifa maxima 

def calcularTarifa(cantViajes,tarifa):
    if cantViajes >=1 and cantViajes <= 20:
        return tarifa
    elif cantViajes >= 21 and cantViajes <= 30:
        return tarifa * 1.2 
    elif cantViajes >= 31 and cantViajes <= 40:
        return tarifa * 1.3
    else:
        return tarifa * 1.4
    

viajesMes = int(input("Ingrese la cantidad de viajes realizados en un mes"))
valorPasaje = int(input("Ingrese el costo del pasaje"))

print("El valor total es:",calcularTarifa(viajesMes,valorPasaje))

