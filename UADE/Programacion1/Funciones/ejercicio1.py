#!/bin/python3

# Desarrollar una funcion que reciba tres numeros positivos y devuelva el mayor de los tres, solo si este es unico (mayor estricto). En caso de no existir el mayor estricto devolver -1. No utilizar operadores logicos (and,or,not). Desarrollar tambien un programa para ingresar los tres valores, invocar a la funcion y mostrar el maximo hallado o un mensaje informativo si este no existe 


def mayorDe3(num1,num2,num3):
    if num1 > num2:
        if num1 > num3:
            return num1
        elif num3 > num1:
            return num3
        else:
            return -1
    elif num2 > num1:
        if num2 > num3:
            return num2
        elif num3 > num2:
            return num3
        else:
            return -1
    else:
        if num3 > num2:
            return num3
        else:
            return -1 

#-----------main program -----------------


numA = int(input('Ingrese numero 1'))
numB = int(input('Ingrese numero 2'))
numC = int(input('Ingrese numero 3'))

numx = mayoresDe3(numA,numB,numC)

if numx == -1:
    print('El valor no existe')
else:
    print('El mayor numero es ',numx)



