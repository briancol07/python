#!/bin/python3

# La siguiente funcion permite averiguar el dia de la semana para una fecha determinada. La fecha se suministra en forma de tres parametros enteros y la funcion devuelve 0 para domingo, 1 para lunes, 2 para martes, etc. Escribir un programa para imprimir por pantalla el calendario de un mes completo, correspondiente a un mes y anio cualquiera basandose en la funcion suministrada. Considerar que la semana comienza en domingo 

##------------ Funcion ----------

def diadelasemana(dia,mes,anio):
    if mes <3: 
        mes = mes + 10 
        anio = anio - 1 
    else: 
        mes = mes - 2 
    siglo = anio // 100
    anio2 = anio % 100
    diasem = (((26 * mes - 2 )//10)+ dia + anio2 + (anio2//4) + (siglo//4) - ( 2 * siglo)) %7
    if diasem < 0:
        diasem = diasem + 7
    return diasem
##---------------------------------
# Faltaria hacer que aparezcan los dias y verificar que todo este correcto ! 
#---------------------------------- 
# enero 31 , feb 28/29 ,marzo 31, abril 30, mayo 31 junio 30, julio 31, agosto 31,septiembre 30,octubre 31, noviembre 30,diembre 31
 
def calendario(mes,anio):
    dias = 0 
    if mes == 2:
        dias = 28
    elif (mes % 2 == 1 and mes <=7) or (mes % 2 == 0 and mes >7):
        dias = 31 
    else:
        dias = 30 
    for i in range(0,dias):
        if i % 7 == 0:
            print("\n")
        print(diadelasemana(i,mes,anio),end=' ')
    print("\n")
    print("\n")
    print("--------------------------------")


calendario(2,2021)
calendario(3,2021)
calendario(4,2021)

