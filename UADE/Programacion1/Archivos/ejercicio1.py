#!/bin/python3 


# 1. Desarrollar un programa para limpiar todos los comentarios de un programa escrito en lenguaje Python. Tener en cuenta que los comentarios comienzan con el signo # (siempre que este no se se encuentre encerrado entre comillas simples o dobles) y que tambien se considera comentario a las cadenas de documentacion (docstrigns) 

f=open('test.py','r')
text = f.readlines()
f.close()

fw=open('test.py', 'w')
for line in text:
    if '#' in line:
        if line.index('#') == 0:
            line = line.replace('#','')
    fw.write(line)
fw.close()
