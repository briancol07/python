# Ejemplo de uso de las estructuras Pila y Cola creadas como clases
from PilaDinamica import Pila
from ColaDinamica import Cola
import random

# Genero una pila con una cantidad de elentos aleatoria entre 0 y 20
p = Pila()
print("Elementos generados:",end=" ")
for x in range(random.randint(0,20)):
    elemento = random.randint(1,100)
    print(elemento, end=" ")
    p.apilar(elemento)
    
print()
print()

# Muestro la lista y genero la cola con los elementos desapilados
c = Cola()
print("Pila generada:", end=" ")
while not p.pilaVacia():
    print(p.tope(), end=" ")
    c.acolar(p.tope())
    p.desapilar()

# Muestro la cola generada
print()
print()
print("Cola generada:", end=" ")
while not c.colaVacia():
    print(c.primero(), end=" ")
    c.desacolar()