# Cola Dinámica

class Cola:
    class Nodo:
        info = None
        sig = None
        
    primerNodo = None
    ultimo = None
    
    def inicializarCola(self):
        """ inicializa la cola, dejandola vacía """
        self.primerNodo = None
        self.ultimo = None
    
    def acolar(self, dato):
        """ agrega un elemento al final de la cola """
        nuevo = self.Nodo()
        nuevo.info = dato
        nuevo.sig = None
        if self.ultimo != None: self.ultimo.sig = nuevo
        self.ultimo = nuevo
        if self.primerNodo == None: self.primerNodo = self.ultimo
    
    def desacolar(self):
        """ remueve el primer elemento de la cola """
        self.primerNodo = self.primerNodo.sig
        if self.primerNodo == None: self.ultimo = None
        
    def colaVacia(self):
        """ verifica si la cola esta vacía """
        return self.ultimo == None
    
    def primero(self):
        """ devuelve el valor del primer elemento de la cola """
        return self.primerNodo.info