# Pila Dinámica

class Pila:
    class Nodo:
        info = None
        sig = None
        
    primero = None
        
    def inicializarPila(self):
        """ inicializa la pila, dejandola vacía """
        self.primero = None
    
    def apilar(self, dato):
        """ agrega un elemento al tope de la pila """
        nuevo = self.Nodo()
        nuevo.valor = dato
        nuevo.sig = self.primero
        self.primero = nuevo
        
    def desapilar(self):
        """ remueve el elemento al tope de la pila """
        self.primero = self.primero.sig
    
    def tope(self):
        """ verifica si la pila esta vacía """
        return self.primero.valor
    
    def pilaVacia(self):
        """ devuelve el valor del primer elemento de la cola """
        return self.primero == None