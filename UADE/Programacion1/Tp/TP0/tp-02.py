#!/bin/python3 

# Sistema de control de stock 
# ----------------------------------------------------------------------------
# Fabrican N productos 
# Mantener un minimo de M unidades 
# No puede almacenar mas de X unidades 

# a. Darle nombre a la frabrica e indicar que producto produce 
# b. Se solicita simular una situacion de stock actual:
#   1. Ingresar por teclado los codigos de productos (4/5 digitos), definirlo en el grupo, ingresar -1 simulando para cada codigo la cantidad en stock con un numero al azar entre dos valores solicitados del teclado 
#   2. Simular tanto el codigo de producto de cuatro digitos como la cantidad inicial en stock con un numero al azar entre dos valores solicitados del teclado 
#   3. Ingresar ambos valores por teclado con los cuidados correspondientes 
#   por supuesto que si el codigo ya existe se debe rechazar por que estamos creando el stock inicial y debe respetar las consignas iniciales respecto a las cantidades de stock  
# 
# 
# 
#----------------------------- imports  ---------------------------------------
import random 

#----------------------------- A ----------------------------------------------

# Nombre de la empresa: MCgiver 

#----------------------------- B ----------------------------------------------

def solicitarCodigos(): 
    codProd = []
    producto = int(input("Ingrese el codigo del producto"))
    while producto != -1:
        while len(str(producto)) < 4 or len(str(producto)) > 5:
            producto = int(input("Ingrese un codigo correcto"))
        if producto not in codProd:
            codProd.append(producto)
        else:
            producto = int(input("Ingrese el codigo del producto"))
    return codProd

def stock(listaProd):
    listaStock = []
    for x in range(len(listaProd)):
        num1 = int(input("ingrese el primer numero"))
        num2 = int(input("ingrese el segundo numero"))
        if num1 <num2: 
            listaStock.append(random.randint(num1,num2))
        elif num1 > num2:
            listaStock.append(random.randint(num2,num1))
        else:
            listaStock.append(random.randint(num1,num2))
    return listaStock

# ----------------------------------------------------------------------------
# Actualizar stock por ventas realizadas o por productos Fabricados 
# c. Se solicita ingresar un codigo de producto, luego ingresar la cantidad vendida. Si el codigo existe actualizar su stock cuidando que existan unidades disponibles para vender, en caso de no alcanzar,emitir un mensaje. Si el codigo no Existe agregarlo como nuevo producto asignado un codigo nuevo de producto(definirl como asignarlo) 

def actualizarStock(codProd,cantVendida):

# ----------------------------------------------------------------------------
# Consultas e informes de producto-Stock actual 
# d. Luego de cada cambio o como opcion a ejecutar cuando lo requiera el usuario se debe poder consular:
    # 1. Listado completo con los codigos y su cantidad correspondiente por pantalla ordenado por codigo de producto 
    # 2. Productos y su cantidad en stock que se encuentran por debajo del stock minimo ordenado por cantidad de stock minimo 
    # 3. Posibilidad de ingresar un codigo de producto e informe cual es su stock actual 
#----------------------------- Main ------------------------------------------

# Se puede pantear un menu de opciones o trabajar con un flujo continuo de ejecucion o investigar una interfaz grafica disenien bien el algoritmo antes de iniciar la programacion Pueden consultar a los profes cuando tienen el disenio acordado 

# Es posible agregar cadena de caracteres. Si el grupo decide agregar mas funcionalidad consultar al docente y asegurar que tendran en tiempo suficiente par terminarlo 

listaCodigos = solicitarCodigos()
print(listaCodigos)
print(stock(listaCodigos))

        




