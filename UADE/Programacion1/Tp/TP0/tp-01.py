################ TP ###############################

# 1.Todos los elementos en posiciones pares o impares
# 2.Invertir todos los elementos de la lista
# 3.Reemplazar, eliminar elementos consecutivos
# 4.Insertar elementos a una lista
# 5.Eliminar todos los elementos no consecutivos en posiciones impares
# 6.Rebanadas nulas para insertar elementos.
# 7.¿Qué sucede si superamos un índice en una rebanada? Para qué sirve

#---------------------------------------------------------------------
# Usar: 
#   * https://www.w3schools.com/python/default.asp
#   * https://developers.google.com/edu/python/lists
#   * http://elclubdelautodidacta.es/wp/2015/12/python-troceando-desde-el-lado-izquierdo/
#   * https://wsvincent.com/python-wat-slice-out-of-range/
#---------------------------------------------------------------------

#------------------- 1 ----------------------------------------

def posPar(lista):
    return lista[::2]

def posImpar(lista):
    return lista[1::2]

# Hay que ver si la posicion cero cuenta como par o impar 

#------------------- 2 ----------------------------------------
def invertir(lista):
    return lista[::-1]

#------------------- 3 ----------------------------------------
def eliminarDesdeHasta(lista,start,end):
    lista[start:end] = []
# def EliminarConsecutivos(lista):
# Si es eliminar elementos desde un punto a otro se pued usar la funcion de arriba 

def ReemplazarConsecutivos(lista,start,end,lista2):
    lista[start,end] = lista2 



#------------------- 4 ----------------------------------------
# Hay 3 formas de agregar elementos:  append,extend,insert
# No hay necesidad de hacerlo como funcion porque ya de por si son pero para separarlos y mostrarlos esta bien 

# Append agrega elemento al final de la lista 

def insertarAppend(lista,elem):
    lista.append(elem)

# Agrega el elemento en la posicion tal 
def insertarInsert(lista,elem,pos):
    lista.insert(pos,elem)

# Agrega elemento al final de la lista pero la une a la lista 
# Ej si tratas con (append y insert) de agregar un elemento compuesto las listas te quedarian lista de listas 
# Encambio con Extend te queda todo como una unica lista

def insertarExtend(lista,elemts):
    lista.extend(elemts)

#------------------- 5 ----------------------------------------
# Aun no esta bien Falta arreglar
def eliminarPosImpar(lista):
    lista[1::2] = []
#------------------- 6 ----------------------------------------
# Las rebanadas nulas sirven para poder insertar en cierto lugar un elemento 
# Todos los ejemplos son con numeros por que son mas simples 

def insertarEnX(lista,pos,num):
    lista[pos:pos] = [num]

#------------------- 7 ----------------------------------------
def outOfRange(lista):
    print("From 0 to out of range: ",lista[0:100])
    print("From last elem (-1) to out of range: ",lista[100:])

# When we ask for an upper bound that is too large, Python defaults to returning the length of the string. So x[0:100] is really x[0:len(x)] which results in x[0:2] which is [1, 2, 3].

# In the second case, x[100:] we’re asking to start at index position 100 and proceed until the end. In this case, Python will return an empty string ''.
#-------------------Main ----------------------------------------

lista = [0,1,2,3,4,5,6,7,8,9,10]

print(posPar(lista))
print(posImpar(lista))

print(invertir(lista))

eliminarDesdeHasta(lista,0,4)
print(lista)

#print(EliminarPosImpar(lista))
insertarEnX(lista,3,100)
print(lista)

outOfRange(lista)
