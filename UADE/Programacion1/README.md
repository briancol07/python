# Programacion 1 


## Temas 

- [x] Funciones 
- [x] Listas 
- [ ] Matrices 
- [ ] Cadena de Caracteres 
- [ ] Manejo de Excepciones 
- [ ] Archivos 
- [ ] Recursividad 
- [ ] Tuplas,Conjuntos y Diccionarios 
- [ ] Tipos Abstractos de Datos 


## Links utiles 

* [ListasPorComprension](https://www.analyticslane.com/2019/09/23/listas-por-comprension-en-python/)

