#!/bin/python3 

# Desarrollar cada una de las siguientes funciones y escribir un programa que permita verificar su funcionamiento, imprimiendo la matriz luego de invocar a cada funcion 

# a. Cargar numeros enteros en una matriz de n x n ingresando los datos desde el teclado 
# b. Ordenar en forma ascendente cada una de las filas de la matriz 
# c. Intercambiar dos filas, cuyos numeros se reciben como parametro
# d. Intercambiar dos columnas dadas , cuyos numeros se reciben como parametro 
# e. Transponer la matriz sobre si misma (intercambiar cada elemento Aij por Aji)
# f. Calcular el promedio de los elementos de una fila cuyo numero se recibe como parametro
# g. Calcular el porcentaje de elementos con valor impar en una columna, cuyo numero se recibe como parametro 
# h. Determinar si la matriz es simetrica con respecto a su diagonal principal 
# i Determinar si la matriz es simetrica con respecto a su diagonal secundaria 
# j. Determinar que columnas de la matriz son palindromos(capicuas), devolviendo una lista con los numeros de las mismas 

# El valor de N debe leerse por teclado. Las funciones deben servir cualquiera sea el valor ingresado 



#----------------------------------- A ----------------------------------------
# Esta funcion me ayuda a la hora de la creacion ya que me permite crear una matriz vacia de n x m 

def crearMatrizVacia(n,m):
    matriz = [] 
    for i in range(n):
        a = [0]*m
        matriz.append(a)
    return matriz 

def cargarMatriz(n,matriz):
    for i in range(n):
        for j in range(n):
            matriz[i][j] = int(input("ingrese un valor"))
    return matriz
            

#----------------------------------- B ----------------------------------------
# b . Ordenar en forma ascendente cada una de las filas de la matriz 

def ordenarMeMa(matriz):
    for x in range(len(matriz)):
        matriz[x].sort()
    return matriz

#----------------------------------- C ----------------------------------------
# c. Intercambiar dos filas, cuyos numeros se reciben como parametro

def switchAB(matriz,a,b):
    aux = []
    aux = matriz[a]
    matriz[a] = matriz[b]
    matriz[b] = aux
    return matriz

#----------------------------------- D ----------------------------------------
# d. Intercambiar dos columnas dadas , cuyos numeros se reciben como parametro 

def switch2(matriz,a,b):
    aux = []
    for x in range(len(matriz)):
        aux = matriz[x][a]
        matriz[x][a] = matriz[x][b]
        matriz[x][b] = aux
    return matriz



#----------------------------------- E ----------------------------------------
# e. Transponer la matriz sobre si misma (intercambiar cada elemento Aij por Aji)

#----------------------------------- F ----------------------------------------
#----------------------------------- G ----------------------------------------
#----------------------------------- H ----------------------------------------
#----------------------------------- I ----------------------------------------
#----------------------------------- J ----------------------------------------


#-------------------------------- Main ----------------------------------------

size = int(input("ingrese N"))
print("creo matriz vacia cuadrada")
print(crearMatrizVacia(size,size))
print(cargarMatriz(size,crearMatrizVacia(size,size)))
matriz1 = cargarMatriz(size,crearMatrizVacia(size,size))
print(ordenarMeMa(matriz1))
print(switchAB(matriz1,0,1))
print(switch2(matriz1,0,1))

