#!/bin/python3 

# Para cada una de las siguientes matrices, desarrolar una funcion que la genere y escribir un progrma que invoque a cada una de ellas y muestre por pantalla la matriz obtenida. El tamanio de las matrices debe establecerse como N x N y las funciones deben servir aunq este valor se modifique  



def crearMatrizCuadrada(n):
    matriz = [] 
    for i in range(n):
        a = [0]*n
        matriz.append(a)
    return matriz 

def mostrarMatriz(matriz):
    for elem in matriz:
        print(elem)

def a(n):
    matriz = [] 
    matriz_aux = []
    num = 1
    for i in range(n):
        for x in range(n):
            if x == i:
                matriz_aux.append(num)
                num+=2
            else:
                matriz_aux.append(0)
        matriz.append(matriz_aux)
        matriz_aux = []
    return matriz

def b(n):
    matriz = [] 
    matriz_aux = []
    num = 0
    for i in range(n):
        for x in range(n):
            if x == n-1-num:
                matriz_aux.append(3**(n-num)//3)
                num += 1
            else:
                matriz_aux.append(0)
        matriz.append(matriz_aux)
        matriz_aux = []
    return matriz
    

def c(n):
    matriz = [] 
    matriz_aux = []
    for i in range(n):
        num = n - i 
        for x in range(n):
            if x == i:
                matriz_aux.append(num)
            elif x < i:
                matriz_aux.append(num)
            else:     
                matriz_aux.append(0)
        matriz.append(matriz_aux)
        matriz_aux = []
    return matriz

def d(n):
    matriz = [] 
    matriz_aux = []
    num = n * 2 
    for i in range(n):
        for x in range(n):
            matriz_aux.append(num)
        matriz.append(matriz_aux)
        matriz_aux = []
        num //= 2
    return matriz

def e(n):
    matriz = [] 
    matriz_aux = []
    num = 1 
    for i in range(n):
        for x in range(n):
            if i % 2 == 0:
                if x % 2 == 1:
                    matriz_aux.append(num)
                    num += 1
                else:
                    matriz_aux.append(0)
            else:
                if x % 2 == 0:
                    matriz_aux.append(num)
                    num += 1
                else:
                    matriz_aux.append(0)
        matriz.append(matriz_aux)
        matriz_aux = []
    return matriz

def f(n):
    matriz = [] 
    matriz_aux = []
    num = 1 
    for i in range(1,n+1):
        for x in range(1,n+1):
            if x <= num:
                matriz_aux.append(num)
                num += 1
            else:
                matriz_aux.append(0) 
        matriz.append(matriz_aux[::-1])
        matriz_aux = []
    return matriz

mostrarMatriz(f(4))
