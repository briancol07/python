#!/bin/python3

import random 

def cargarCine(filas,butacas):
    cine = [0 for n in range(filas)]
    cine2 = [cine for n in range(butacas)]
    return cine2

def reservar(matriz):
    x = int(input("ingrese la fila que desea "))
    y = int(input("ingrese la columna que desea "))
    # Falta hacer si esta fuera de rango 
    if matriz[x][y] == 0:
        matriz[x][y] = 1;
        return True 
    else:
        return False 

filas = random.randint(1,10)
butacas = random.randint(1,10)
matriz = cargarCine(filas,butacas)
print(matriz)
reservar(matriz)
print(matriz)


